const mysql = require('mysql2/promise');
// const moment = require('moment');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');
const controlAuth = require('../controlauth/controlAuth.js');
const fs = require('fs');

async function getStaffList(data, res) {

  function validate(login) {
    return f_lib.strings.validateLoginsPasswords(login);
  }

  async function getStaff(login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(
        `SELECT * FROM users WHERE id IN (
          SELECT user FROM users_access WHERE admin=${user[0].id}
        ) OR id=${user[0].id}`
      );
      let [rows1, fields1] = await conn.execute(`SELECT * FROM users WHERE id=${user[0].id}`);
      let fulllist = [];
      for (let i = 0; i < rows.length; i++) {

        let [wpForUsers, wfFields] = await conn.execute(
          `SELECT * FROM users_workpositions WHERE user=${rows[i].id}`
        );

        let userWP = { 'wp': wpForUsers.length > 0 ? await f_lib.db.wpById(wpForUsers[0].wp) : null };

        if (rows[i].type == 2) {
          let [functionalsForModerator, ffmField] = await conn.execute(
            `SELECT * FROM moderator_access WHERE useraccess IN (
              SELECT id FROM users_access WHERE user=${rows[i].id}
            )`
          );

          let moderatorAccessList = {};

          f_lib.fullAccessList().forEach((el) => {
            moderatorAccessList[el] = functionalsForModerator[0][el];
          });

          fulllist.push({
            ...rows[i],
            ...moderatorAccessList,
            ...userWP
          });
        }
        else {
          fulllist.push({
            ...rows[i],
            ...userWP
          });
        }
      }

      await conn.end();
      return fulllist;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  // if(!f_lib.objects.isSetData(data)){
  //   res.send({
  //     status: false,
  //     data: 
  //   });
  //   return false;
  // }

  if (!validate(data.login)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  let userslist = await getStaff(data.login);
  // console.log(userslist);

  res.send({
    status: true,
    content: userslist
  });

  return true;
}

async function deleteUserFromSystem(globaaldir, data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.login) &&
      f_lib.strings.validateLoginsPasswords(data.loginModer) &&
      f_lib.strings.validateDataNums(data.deleteId)
    );
  }

  async function isDeleteLegal(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users WHERE id=${id}`);
      await conn.end();
      return rows[0].type != 1;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteUserFromSystem(login, id) {
    // let user = await f_lib.db.userByLogin(login);

    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users WHERE id=${id}`);
      let path = rows[0].dir;
      if (fs.existsSync(`${globaaldir}/users_file_system/${path}/logo/`)) {

        let files = fs.readdirSync(`${globaaldir}/users_file_system/${path}/logo/`);
        if (files.length > 0) {
          files.forEach((el, i) => {
            fs.unlinkSync(`${globaaldir}/users_file_system/${path}/logo/${el}`);
          });
        }
        fs.rmdirSync('users_file_system/' + path + '/logo');
        fs.rmdirSync('users_file_system/' + path);
      }
      await conn.execute(`DELETE FROM users WHERE id=${id}`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isUserCreatedByThisAdmin(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Укажите все данные.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  if (!await isDeleteLegal(data.deleteId)) {
    res.send({
      status: false,
      content: 'Нельзя удалять администрторов.'
    });
    return false;
  }

  let userData = await f_lib.db.userByLogin(data.login);
  let userDataModer = await f_lib.db.userByLogin(data.loginModer);

  if (data.deleteId == userDataModer[0].id) {
    res.send({
      status: false,
      content: 'Нельзя удалить самого себя.'
    });
    return false;
  }

  if (!await isUserCreatedByThisAdmin(userData[0].id, data.deleteId)) {
    res.send({
      status: false,
      content: 'Этот пользователь создан не вами.'
    });
    return false;
  }

  let resDelete = await deleteUserFromSystem(data.login, data.deleteId);
  if (resDelete) {
    res.send({
      status: true,
      content: 'Пользователь удалён.'
    });
    return true;
  }

  return true;
}

async function changeUserType(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.login) &&
      f_lib.strings.validateLoginsPasswords(data.loginModer) &&
      f_lib.strings.validateDataNums(data.changeUser) &&
      (data.typeModerator === true || data.typeModerator === false)
    );
  }

  async function isRedactedUserModerator(data) {
    let user = await f_lib.db.userByLogin(data.login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${user[0].id} AND user=${data.changeUser}`);
      if (rows.length > 0) {
        let [rows1, fields1] = await conn.execute(`SELECT * FROM moderator_access WHERE useraccess=${rows[0].id}`);
        return rows1.length > 0;
      }
      await conn.end();
      return false;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isUserCreatedByThisAdmin(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function changeUserTypeInDB(adminId, userId, key) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = null;
      let [rows_user, fields_user] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      if (key === true) {
        await conn.execute(`UPDATE \`users\` SET \`type\`='${await f_lib.db.userTypeByName('moderator')}' WHERE \`id\` = '${userId}'`);
        result = await conn.execute(`INSERT INTO \`moderator_access\` (\`useraccess\`,\`redactforms\`, \`redactstaff\`, \`makeevents\`) VALUES ('${rows_user[0].id}', '${0}', '${0}', '${0}');`);
      }
      else {
        await conn.execute(`UPDATE \`users\` SET \`type\`='${await f_lib.db.userTypeByName('user')}' WHERE \`id\` = '${userId}'`);
        await conn.execute(`DELETE FROM moderator_access WHERE useraccess=${rows_user[0].id}`);
      }
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны нерректно.'
    });
    return;
  }

  let user = await f_lib.db.userByLogin(data.login);
  if (!await isUserCreatedByThisAdmin(user[0].id, data.changeUser)) {
    res.send({
      status: false,
      content: 'Этот пользователь создан не вами.'
    });
    return false;
  }

  if (!await controlAuth.checkUserType(data.login, ['admin', 'moderator'])) {
    res.send({
      status: false,
      content: 'Вы не можете осуществить данную операцию.'
    });
    return;
  }

  // console.log(await f_lib.db.userById(data.changeUser));



  if (await controlAuth.checkUserType(await f_lib.db.userById(data.changeUser), ['admin'])) {
    res.send({
      status: false,
      content: 'Нельзя изменять данные учётной записи администратора.'
    });
    return;
  }


  // let userDataModer = await f_lib.db.userByLogin(data.loginModer);

  // if(data.changeUser == userDataModer[0].id){
  //   res.send({
  //     status: false,
  //     content: 'Нельзя удалить самого себя.'
  //   });
  //   return false;
  // }  

  if (data.typeModerator == true) {
    if (await isRedactedUserModerator(data)) {
      res.send({
        status: false,
        content: 'Модератор уже назначен.'
      });
      return;
    }

    let resChange = await changeUserTypeInDB(user[0].id, data.changeUser, data.typeModerator);
    if (resChange) {
      res.send({
        status: true,
        content: true
      });
    }
  }
  else if (data.typeModerator == false) {
    if (!await isRedactedUserModerator(data)) {
      res.send({
        status: false,
        content: 'Модератор не найден.'
      });
      return;
    }

    let resChange = await changeUserTypeInDB(user[0].id, data.changeUser, data.typeModerator);
    if (resChange) {
      res.send({
        status: true,
        content: false
      });
    }
  }

  return true;
}

async function getModeratorAccessList(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.login) &&
      f_lib.strings.validateDataNums(data.moderator)
    );
  }

  async function isUserCreatedByThisAdmin(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function getModeratorAccessFullList(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT redactforms, redactstaff, makeevents FROM moderator_access WHERE useraccess IN (
        SELECT id FROM users_access WHERE admin=${adminId} AND user=${userId}
      )`);
      await conn.end();
      return rows[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны нерректно.'
    });
    return;
  }

  let user = await f_lib.db.userByLogin(data.login);
  if (!await isUserCreatedByThisAdmin(user[0].id, data.moderator)) {
    res.send({
      status: false,
      content: 'Этот пользователь создан не вами.'
    });
    return false;
  }

  let accessList = await getModeratorAccessFullList(user[0].id, data.moderator);
  res.send({
    status: true,
    content: accessList
  });

  return;
}

async function changeModeratorAccessList(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.login) &&
      f_lib.strings.validateDataNums(data.moderator) &&
      (data.checkedstatus === true || data.checkedstatus === false) &&
      f_lib.fullAccessList().includes(data.functional)
    );
  }

  async function isUserCreatedByThisAdmin(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function resetModeratorAccessList(adminId, userId, functional, status) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`UPDATE \`moderator_access\` SET \`${functional}\`='${(status ? 1 : 0)}' WHERE \`useraccess\` IN (
        SELECT id FROM users_access WHERE admin=${adminId} AND user=${userId}
      )`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны нерректно.'
    });
    return;
  }

  let user = await f_lib.db.userByLogin(data.login);
  if (!await isUserCreatedByThisAdmin(user[0].id, data.moderator)) {
    res.send({
      status: false,
      content: 'Этот пользователь создан не вами.'
    });
    return false;
  }

  // let userDataModerator = await f_lib.db.userByLogin(data.loginAuthUser);

  // if(userDataModerator[0].id == data.moderator){
  //   res.send({
  //     status: false,
  //     content: 'Управлять вашими правами доступа может только администртор, или другие модераторы, имеющие доступ.'
  //   });
  //   return false;
  // }  

  let resReset = await resetModeratorAccessList(
    user[0].id,
    data.moderator,
    data.functional,
    data.checkedstatus
  );

  res.send({
    status: true,
    content: true
  });

  return;
}

async function connectUserWithWP(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.user) &&
      f_lib.strings.validateDataNums(data.wp)
    );
  }

  async function isUserCreatedByThisAdmin(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isWPCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteOldConnects(user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM users_workpositions WHERE user=${user}`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addNewConnection(user, wp) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`INSERT INTO users_workpositions (user, wp) VALUES (${user}, ${wp})`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны нерректно.'
    });
    return;
  }

  let user = await f_lib.db.userByLogin(data.userlogin);
  if (user[0].type != 1 && user[0].id == data.user) {
    if (!await isUserCreatedByThisAdmin(user[0].id, data.user)) {
      res.send({
        status: false,
        content: 'Этот пользователь создан не вами.'
      });
      return false;
    }
  }

  if (!await isWPCreatedByThisUser(data.wp, user[0].login)) {
    res.send({
      status: false,
      content: 'Эта должность создана не вами.'
    });
    return false;
  }

  await deleteOldConnects(data.user);
  await addNewConnection(data.user, data.wp);

  res.send({
    status: true,
    content: true
  });
}

async function disconnectUserAndWP(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.user)
    );
  }

  async function isUserCreatedByThisAdmin(adminId, userId) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE admin=${adminId} AND user=${userId}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteOldConnects(user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM users_workpositions WHERE user=${user}`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны нерректно.'
    });
    return;
  }

  let user = await f_lib.db.userByLogin(data.userlogin);
  if (user[0].type != 1 && user[0].id == data.user) {
    if (!await isUserCreatedByThisAdmin(user[0].id, data.user)) {
      res.send({
        status: false,
        content: 'Этот пользователь создан не вами.'
      });
      return false;
    }
  }

  await deleteOldConnects(data.user);

  res.send({
    status: true,
    content: true
  });
}

module.exports.getStaffList = getStaffList;
module.exports.deleteUserFromSystem = deleteUserFromSystem;
module.exports.changeUserType = changeUserType;
module.exports.getModeratorAccessList = getModeratorAccessList;
module.exports.changeModeratorAccessList = changeModeratorAccessList;
module.exports.connectUserWithWP = connectUserWithWP;
module.exports.disconnectUserAndWP = disconnectUserAndWP;