const mysql = require('mysql2/promise');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');

async function addUG(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let newUGrow = null;

  data.nameUG = f_lib.strings.changeDataNames(data.nameUG);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let ug = f_lib.strings.validateDataNames(data.nameUG);
    let copyUG = true;
    if (data.copyIdUG) {
      copyUG = f_lib.strings.validateDataNums(data.copyIdUG);
    }
    return login && ug && copyUG;
  }

  async function isSetUG(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addUGtoDB(name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = await conn.execute(`INSERT INTO \`users_groups\` (\`user\`, \`name\`) VALUES ('${user[0].id}', '${name}');`);
      let getNewUGById = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${result[0].insertId}'`);
      await conn.end();
      return getNewUGById[0][0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isUGCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function makeCloneUG(baseWPid, cloneId, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rowsWPConnects, fieldsWPConnects] = await conn.execute(`SELECT * FROM \`wpconnections\` WHERE \`wp\` = '${baseWPid}'`);
      rowsWPConnects.forEach(async element => {
        await conn.execute(`INSERT INTO \`wpconnections\` (\`wp\`, \`competence\`) VALUES ('${cloneId}', '${element.competence}');`);
      });
      // await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetUG(data.nameUG, data.login)) {
        let newug = await addUGtoDB(data.nameUG, data.login);
        newUGrow = newug;

        // if (data.copyIdUG) {
        //   if (await isUGCreatedByThisUser(data.copyIdUG, data.login)) {
        //     await makeCloneUG(data.copyIdUG, newUGrow.id, data.login);
        //   }
        // }
      }
      else {
        counterErrors = 1;
        errorsList.push('Подразделение с таким названием уже существует.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название подразделения может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: newUGrow
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function searchUG(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  data.nameUG = f_lib.strings.changeDataNames(data.nameUG);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let ug = f_lib.strings.validateDataNames(data.nameUG) || (data.nameUG == '');
    let searchByAlphabet = data.sortabc == 'true' || data.sortabc == 'false';
    return login && ug && searchByAlphabet;
  }

  async function searchUGinDB(name, login, sortabc) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`user\` = '${user[0].id}' AND \`name\` LIKE '%${name}%' ${sortabc == 'true' ? ' ORDER BY \`name\` ASC' : ''}`);
      await conn.end();
      return rows;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }
  let resultSearch = [];
  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      resultSearch = await searchUGinDB(data.nameUG, data.login, data.sortabc);
    }
    else {
      counterErrors = 1;
    }
  }
  else {
    counterErrors = 1;
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resultSearch
    });
  }
  else {
    res.send({
      status: false,
      content: resultSearch
    });
  }
}

async function deleteUG(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  function validate(data) {
    let id = f_lib.strings.validateDataNums(data.id);
    let login = f_lib.strings.validateLoginsPasswords(data.login);
    return id && login;
  }

  async function isUGCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteUG(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM \`users_groups\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isUGCreatedByThisUser(data.id, data.login)) {
        await deleteUG(data.id);
      }
      else {
        counterErrors = 1;
        errorsList.push('У вас нет доступа для редактирования этих данных.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Данные указаны некорректно.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Данные не указаны.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: 'Данные удалены'
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function renameUG(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let resrename = null;

  data.nameUG = f_lib.strings.changeDataNames(data.nameUG);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let ug = f_lib.strings.validateDataNames(data.nameUG);
    let id = f_lib.strings.validateDataNums(data.id);
    return login && ug && id;
  }

  async function isUGCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetUGinDB(id, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.id == id
      }).length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetUG(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function renameUGinDB(id, name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`UPDATE \`users_groups\` SET \`name\`='${name}' WHERE \`id\` = '${id}' AND \`user\`= '${user[0].id}'`);
      let getNewUGById = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return getNewUGById[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetUGinDB(data.id, data.login)) {
        if (await isSetUG(data.nameUG, data.login)) {
          if (await isUGCreatedByThisUser(data.id, data.login)) {
            resrename = await renameUGinDB(data.id, data.nameUG, data.login);
          }
          else {
            counterErrors = 1;
            errorsList.push('У вас нет доступа для редактирования этих данных.');
          }
        }
        else {
          counterErrors = 1;
          errorsList.push('подразеделение с таким названием уже существует.');
        }
      }
      else {
        counterErrors = 1;
        errorsList.push('Редактируемое подразеделение не найдена.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название подразеделения может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resrename
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function searchConnects(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateDataNums(data.ug) && f_lib.strings.validateLoginsPasswords(data.login)
    );
  }

  async function isUGCreatedByThisUser(data) {
    let user = await f_lib.db.userByLogin(data.login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getUG = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${data.ug}' AND \`user\`='${user[0].id}'`);
      await conn.end();
      return getUG[0].length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function getFullConnectsList(data) {
    let user = await f_lib.db.userByLogin(data.login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getUG = await conn.execute(`SELECT * FROM \`users_groups_connections\` WHERE \`usergroup\` = '${data.ug}'`);
      await conn.end();
      return getUG[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные не указаны.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны неверно.'
    });
    return false;
  }

  if (!await isUGCreatedByThisUser(data)) {
    res.send({
      status: false,
      content: 'У вас нет прав для получения этой информации. 123'
    });
    return false;
  }

  let fullList = await getFullConnectsList(data);

  res.send({
    status: true,
    content: fullList
  });
  return true;
}

async function connectUserToGroup(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.user) &&
      f_lib.strings.validateDataNums(data.ug)
    );
  }

  async function isUserConnectedToGroup(login, ug, user_id, res) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getUG = await conn.execute(`SELECT * FROM users_groups WHERE id = '${ug}' AND user = '${user[0].id}'`);
      let getUser = await conn.execute(`SELECT * FROM users WHERE id = '${user_id}' AND id IN (
        SELECT user FROM users_access WHERE admin = '${user[0].id}'
      )`);
      let isSetConnect = await conn.execute(`SELECT * FROM \`users_groups_connections\` WHERE \`usergroup\` = '${ug}' AND \`user\` = '${user_id}'`);

      let res1 = getUser[0].length != 0;
      let res2 = user[0].id == user_id;

      if (!((getUG[0].length != 0) && (res1 || res2))) {
        res.send({
          status: false,
          content: 'Подразделение и сотрудник созданы разными пользователями.'
        });
        return false;
      }

      if (isSetConnect[0].length != 0) {
        res.send({
          status: false,
          content: 'Данная связка уже существует.'
        });
        return false;
      }

      await conn.end();

      return true;

    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addConnect(ug, user) {    
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = await conn.execute(`INSERT INTO \`users_groups_connections\` (\`usergroup\`, \`user\`) VALUES ('${ug}', '${user}');`);
      let getNewConnectById = await conn.execute(`SELECT * FROM \`users_groups\` WHERE \`id\` = '${result[0].insertId}'`);
      await conn.end();
      return getNewConnectById[0][0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }


  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные не указаны.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны неверно.'
    });
    return false;
  }

  if (!await isUserConnectedToGroup(data.userlogin, data.ug, data.user, res)) {
    return false;
  }

  let resAddConnect = await addConnect(data.ug, data.user);

  res.send({
    status: true,
    content: resAddConnect
  });

  return true;
}

async function disconnectUserFromGroup(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.user) &&
      f_lib.strings.validateDataNums(data.ug)
    );
  }

  async function isUserConnectedToGroup(login, ug, user_id, res) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getUG = await conn.execute(`SELECT * FROM users_groups WHERE id = '${ug}' AND user = '${user[0].id}'`);
      let getUser = await conn.execute(`SELECT * FROM users WHERE id = '${user_id}' AND id IN (
        SELECT user FROM users_access WHERE admin = '${user[0].id}'
      )`);
      let isSetConnect = await conn.execute(`SELECT * FROM \`users_groups_connections\` WHERE \`usergroup\` = '${ug}' AND \`user\` = '${user_id}'`);

      let res1 = getUser[0].length != 0;
      let res2 = user[0].id == user_id;

      if (!((getUG[0].length != 0) && (res1 || res2))) {
        res.send({
          status: false,
          content: 'Подразделение и сотрудник созданы разными пользователями.'
        });
        return false;
      }

      if (!(isSetConnect[0].length != 0)) {
        res.send({
          status: false,
          content: 'Данная связка не существует.'
        });
        return false;
      }

      await conn.end();

      return true;

    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteConnect(ug, user) {    
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM users_groups_connections WHERE usergroup = '${ug}' AND user = '${user}'`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }


  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные не указаны.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны неверно.'
    });
    return false;
  }

  if (!await isUserConnectedToGroup(data.userlogin, data.ug, data.user, res)) {
    return false;
  }

  let resdeleteConnect = await deleteConnect(data.ug, data.user);

  res.send({
    status: true,
    content: resdeleteConnect
  });

  return true;
}

module.exports.addUG = addUG;
module.exports.searchUG = searchUG;
module.exports.deleteUG = deleteUG;
module.exports.renameUG = renameUG;
module.exports.searchConnects = searchConnects;
module.exports.connectUserToGroup = connectUserToGroup;
module.exports.disconnectUserFromGroup = disconnectUserFromGroup;