const mysql = require('mysql2/promise');
const moment = require('moment');
const crypto = require('crypto');

const conf = require('./config.js');

let strings = {
  minLength: function (value, num) {
    return value.length >= num;
  },
  maxLength: function (value, num) {
    return value.length <= num;
  },
  validateLoginsPasswords: function (value) {
    return new String(value).match(conf.validate.passwords) ? true : false;
  },
  validateNames: function (value) {
    return new String(value).match(conf.validate.names) ? true : false;
  },
  validateDataNames: function (value) {
    // return new String(value).match(conf.validate.dataNames);
    return (true && new String(value).length >= 1) ? true : false;
  },
  validateDataNums: function (value) {
    return new String(value).match(conf.validate.nums) ? true : false;
  },
  changeDataNames: function (value) {
    let forbiddenSymbols = ['\`', '\'', '\;', '\$', '\*', '\:'];
    return value.split('').filter(el => !forbiddenSymbols.includes(el)).join('');
  }
};

let crypt = {
  createSHA256: function (strToCrypt) {
    let createHash = crypto.createHash("sha256");
    createHash.update(strToCrypt);
    return createHash.digest("base64");
  }
};

let objects = {
  isSetData: function (userList) {
    return Object.values(userList).every(function (field, i) {
      return typeof field != 'undefined';
    });
  },
};

let db = {
  userByLogin: async function (userlogin, message, res) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${userlogin}'`);
      await conn.end();
      return rows;
    } catch (error) {
      // if(messege && res){
      //   message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      //   message.status = false;
      //   res.send(message);
      // }
      throw error;
    }
  },
  userById: async function (id, message, res) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows;
    } catch (error) {
      // if(messege && res){
      //   message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      //   message.status = false;
      //   res.send(message);
      // }
      throw error;
    }
  },
  wpById: async function (id, message, res) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}'`);
      await conn.end();
      if (rows.length > 0) {
        return rows[0].name;
      }
      else {
        return 'Должность не найдена.'
      }
    } catch (error) {
      // if(messege && res){
      //   message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      //   message.status = false;
      //   res.send(message);
      // }
      throw error;
    }
  },
  isSetLoginInDB: async function (userlogin, message, res) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${userlogin}'`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      // message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      // message.status = false;
      // res.send(message);
      throw error;
    }
  },
  userTypeByName: async function (type) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_types\` WHERE \`type\` = '${type}'`);
      await conn.end();
      return rows[0].id;
    } catch (error) {
      let messege = {};
      // message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      // message.status = false;
      // res.send(message);
      throw error;
    }
  }
};

let time = {
  formatTime: function (time, timeNow) {

    let newFormat = {
      '01': '13',
      '02': '14',
      '03': '15',
      '04': '16',
      '05': '17',
      '06': '18',
      '07': '19',
      '08': '20',
      '09': '21',
      '10': '22',
      '11': '23',
      '12': '00'
    };

    if (timeNow == 'am') {
      let newtime = time.split('');
      let searchingElement = newtime[8] + '' + newtime[9];
      if (searchingElement == '12') {
        newtime[8] = newFormat[searchingElement].charAt(0);
        newtime[9] = newFormat[searchingElement].charAt(1);
        time = newtime.join('');
      }
    }

    if (timeNow == 'pm') {
      let newtime = time.split('');
      let searchingElement = newtime[8] + '' + newtime[9];
      if (searchingElement != '12') {
        newtime[8] = newFormat[searchingElement].charAt(0);
        newtime[9] = newFormat[searchingElement].charAt(1);
        time = newtime.join('');
      }
    }

    return time;
  }
}

let fullAccessList = () => {
  return (
    [
      'redactforms',
      'redactstaff',
      'makeevents'
    ]
  );
}

module.exports.fullAccessList = fullAccessList;
module.exports.strings = strings;
module.exports.objects = objects;
module.exports.crypt = crypt;
module.exports.db = db;
module.exports.time = time;