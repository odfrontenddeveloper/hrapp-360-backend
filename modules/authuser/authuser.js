const mysql = require('mysql2/promise');
const fs = require('fs');
const crypto = require('crypto');
const shortid = require('shortid');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const conf = require('../config.js');
const f_lib = require('../func_lib.js');

async function authUser(userData, res) {

  let userInfo = {
    login: userData.login,
    password: userData.password
  };

  let counterErrors = 0;

  let message = {
    login: [],
    loginvalue: userInfo.login,
    password: [],
    total: '',
    status: false
  };

  function checkCounterErrors(errorsList){
    let counter = 0;
    for(let field in errorsList){
      if(!['total','status','token','loginvalue','usertype'].includes(field)){
        if(errorsList[field].length != 0){
          counter++;
        }
      }
    }
    return counter;
  }

  function validate(userList) {
    !f_lib.strings.validateLoginsPasswords(userList.login) ? (
      message.login.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n') ) : ( false );

    !(f_lib.strings.minLength(userList.login, 6) && f_lib.strings.maxLength(userList.login, 30)) ? (
      message.login.push('Допустимое количество символов в логине - от 6 до 30.\n') ) : ( false );

    !(f_lib.strings.minLength(userList.password, 6) && f_lib.strings.maxLength(userList.password, 30)) ? (
      message.password.push('Допустимое количество символов в пароле - от 6 до 30.\n') ) : ( false );

    !f_lib.strings.validateLoginsPasswords(userList.password) ? (
      message.password.push('Пароль может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n') ) : ( false );

    if(checkCounterErrors(message) == 0){
      return true;
    }
    return false;
  }

  async function isPasswordTrue(userlogin, userpassword) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${userlogin}'`);
      let pass = f_lib.crypt.createSHA256(userpassword);
      if(rows.length > 0){
        if(pass == rows[0].password){
          return true;
        }
      }
      return false;
      await conn.end();
    } catch(error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  async function createUserToken(token, dateCreateToken, login){
    try {
      let userid = await f_lib.db.userByLogin(login);
      let conn = await mysql.createConnection(conf.dbconfig);
      let timeNow = moment().format('a');
      let thFormat = String(timeNow) == 'pm' ? 1 : 0;
      let resInsert = await conn.execute(`INSERT INTO \`tokens\`(\`user\`, \`token\`, \`date\`) VALUES (${userid[0].id},'${token}','${String(dateCreateToken)}')`);
      await conn.end();
      return 1;
    } catch (error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  async function getTypeById(id){
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_types\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows[0].type;
    } catch(error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  async function userType(userlogin){
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${userlogin}'`);
      await conn.end();
      if(rows) {
        let typeID = rows[0].type;
        let typeName = await getTypeById(typeID);
        return typeName;
      }
    } catch(error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  async function authUserAccount(userList){
    let jwtconfig = { login: userList.login, password: userList.password };
    let token = jwt.sign(jwtconfig, 'shhhhh');
    message.token = token;
    let dateCreateToken = f_lib.time.formatTime(moment().format('YYYYMMDDhhmmss'), moment().format('a'));
    let res_createUserToken = await createUserToken(token, dateCreateToken, userList.login);
    return 1;
  }

  let resauth = 0;

  if(f_lib.objects.isSetData(userInfo)){
    if(validate(userInfo)){
      if(checkCounterErrors(message) == 0){
        let isLoginInDB = await f_lib.db.isSetLoginInDB(userInfo.login);
        let m_isLoginInDB = isLoginInDB ? '' : 'Указанный логин не существует.\n';
        if(!isLoginInDB){
          message.login.push(m_isLoginInDB);
        }
        if(checkCounterErrors(message) == 0){
          let ispassword = await isPasswordTrue(userInfo.login, userInfo.password);
          let m_ispassword = ispassword ? '' : 'Неверный пароль.\n';
          if(!ispassword){
            message.password.push(m_ispassword);
          }
          if(checkCounterErrors(message) == 0){
            resauth = await authUserAccount(userInfo);
          }
        }
      }
    }
  }

  message.total = resauth == 1 ? 'Поздравляем! Вы - авторизованный пользоватль!.' : '';
  message.status = checkCounterErrors(message) == 0 && resauth == 1;
  if(checkCounterErrors(message) == 0){
    message.usertype = await userType(userInfo.login);
  }

  res.send(message);
}

module.exports.authUser = authUser;
