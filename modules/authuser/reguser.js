const mysql = require('mysql2/promise');
const fs = require('fs');
const crypto = require('crypto');
const shortid = require('shortid');

const conf = require('../config.js');
const f_lib = require('../func_lib.js');

async function regUser(userData, res) {

  let userInfo = {
    login: userData.login,
    name: userData.name,
    surename: userData.surename,
    patronymic: userData.patronymic,
    password: userData.password,
    repeatpassword: userData.repeatpassword
  };

  let counterErrors = 0;

  let message = {
    login: [],
    name: [],
    surename: [],
    patronymic: [],
    password: [],
    repeatpassword: [],
    samepasswords: [],
    total: '',
    status: false
  };

  function checkCounterErrors(errorsList) {
    let counter = 0;
    for (let field in errorsList) {
      if (field != 'total' && field != 'status') {
        if (errorsList[field].length != 0) {
          counter++;
        }
      }
    }
    return counter;
  }

  function samePasswords(pass1, pass2) {
    return pass1 == pass2;
  }

  function validate(userList) {
    !f_lib.strings.validateLoginsPasswords(userList.login) ? (
      message.login.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n')) : (false);

    !(f_lib.strings.minLength(userList.login, 6) && f_lib.strings.maxLength(userList.login, 30)) ? (
      message.login.push('Допустимое количество символов в логине - от 6 до 30.\n')) : (false);

    !(f_lib.strings.minLength(userList.name, 1) && f_lib.strings.maxLength(userList.name, 30)) ? (
      message.name.push('Допустимое количество символов в имени - от 1 до 30.\n')) : (false);

    !(f_lib.strings.minLength(userList.surename, 1) && f_lib.strings.maxLength(userList.surename, 30)) ? (
      message.surename.push('Допустимое количество символов в фамилии - от 1 до 30.\n')) : (false);

    !(f_lib.strings.minLength(userList.patronymic, 1) && f_lib.strings.maxLength(userList.patronymic, 30)) ? (
      message.patronymic.push('Допустимое количество символов в отчестве - от 1 до 30.\n')) : (false);

    !f_lib.strings.validateNames(userList.name) ? (
      message.name.push('Имя может состоять из букв латинского, русского и украинского алфавитов, цифр и знака подчёркивания.\n')) : (false);

    !f_lib.strings.validateNames(userList.surename) ? (
      message.surename.push('Фамилия может состоять из букв латинского, русского и украинского алфавитов, цифр и знака подчёркивания.\n')) : (false);

    !f_lib.strings.validateNames(userList.patronymic) ? (
      message.patronymic.push('Отчество может состоять из букв латинского, русского и украинского алфавитов, цифр и знака подчёркивания.\n')) : (false);

    !(f_lib.strings.minLength(userList.password, 6) && f_lib.strings.maxLength(userList.password, 30)) ? (
      message.password.push('Допустимое количество символов в пароле - от 6 до 30.\n')) : (false);

    !(f_lib.strings.minLength(userList.repeatpassword, 6) && f_lib.strings.maxLength(userList.repeatpassword, 30)) ? (
      message.repeatpassword.push('Пароль может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n')) : (false);

    !f_lib.strings.validateLoginsPasswords(userList.repeatpassword) ? (
      message.repeatpassword.push('Допустимое количество символов в пароле - от 6 до 30.\n')) : (false);

    !f_lib.strings.validateLoginsPasswords(userList.password) ? (
      message.password.push('Пароль может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n')) : (false);

    !samePasswords(userList.password, userList.repeatpassword) ?
      message.samepasswords.push('Пароли не совпадают.') : (false);

    if (userData.createByAdmin) {
      !f_lib.strings.validateLoginsPasswords(userData.createByAdmin) ? (message.login.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n')) : (false);
    }

    if (checkCounterErrors(message) == 0) {
      return true;
    }
    return false;
  }


  async function isSetLoginInDB(userlogin) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${userlogin}'`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  async function createUserAccount(userList) {
    try {
      let resultHashPassword = f_lib.crypt.createSHA256(userList.password);
      let resultHashLogin = f_lib.crypt.createSHA256(userList.login);
      let newdir = resultHashLogin.split('').map(function (el, i) {
        if (!['/', '.', '\\', '='].includes(el)) {
          return el;
        }
      }).join('');
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = await conn.execute(`INSERT INTO \`users\`(\`login\`, \`password\`, \`name\`, \`surename\`, \`patronymic\`, \`dir\`, \`type\`) VALUES ('${userList.login}','${resultHashPassword}','${userList.name}','${userList.surename}','${userList.patronymic}','${newdir}',${userData.createByAdmin ? await f_lib.db.userTypeByName('user') : await f_lib.db.userTypeByName('admin')})`);
      let newUser = await conn.execute(`SELECT * FROM \`users\` WHERE \`id\` = '${result[0].insertId}'`);
      if (userData.createByAdmin) {
        let userAdmin = await f_lib.db.userByLogin(userData.createByAdmin);
        await conn.execute(`INSERT INTO \`users_access\` (\`admin\`, \`user\`) VALUES ('${userAdmin[0].id}', '${result[0].insertId}')`);
      }
      await conn.end();
      if (!fs.existsSync('users_file_system/' + newdir)) {
        fs.mkdirSync('users_file_system/' + newdir);
        fs.mkdirSync('users_file_system/' + newdir + '/logo');
        if (!userData.createByAdmin) {
          fs.mkdirSync('users_file_system/' + newdir + '/assessmentforms');
          fs.mkdirSync('users_file_system/' + newdir + '/results');
        }
      }
      return { ...newUser[0][0], wp: null };
    } catch (error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  let resreg = 0;

  if (f_lib.objects.isSetData(userInfo)) {
    if (validate(userInfo)) {
      if (checkCounterErrors(message) == 0) {
        let isLoginInDB = await isSetLoginInDB(userInfo.login);
        let m_isLoginInDB = isLoginInDB ? 'Пользователь с таким логином уже зарегистрирован.\n' : '';
        if (isLoginInDB == true) {
          message.login.push(m_isLoginInDB);
        }
        if (checkCounterErrors(message) == 0) {
          resreg = await createUserAccount(userInfo);
        }
      }
    }
  }

  message.total = resreg ? resreg : '';
  message.status = checkCounterErrors(message) == 0;

  // res.setHeader("Set-Cookie", "qwerty=219ffwef9w0f; Domain=somecompany.co.uk; Path=/; Expires=Wed, 30 Aug 2019 00:00:00 GMT");
  // console.log(res);
  res.send(message);
}

module.exports.regUser = regUser;
