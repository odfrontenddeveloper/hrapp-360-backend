const mysql = require('mysql2/promise');
const moment = require('moment');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');

async function checkToken(data, res) {
  try {
    let userInfo = {
      login: data.login,
      token: data.token
    };

    let message = {
      login: [],
      token: [],
      status: true
    };

    function checkCounterErrors(errorsList) {
      let counter = 0;
      for (let field in errorsList) {
        if (!['status'].includes(field)) {
          if (errorsList[field].length != 0) {
            counter++;
          }
        }
      }
      return counter;
    }

    function validate(userInfo) {
      !f_lib.strings.validateLoginsPasswords(userInfo.login) ? (
        message.login.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания.\n')) : (false);
      !(f_lib.strings.minLength(userInfo.login, 6) && f_lib.strings.maxLength(userInfo.login, 30)) ? (
        message.login.push('Допустимое количество символов в логине - от 6 до 30.\n')) : (false);

      if (checkCounterErrors(message) == 0) {
        return true;
      }
      return false;
    }

    async function getUserTokensList(userid) {
      try {
        let conn = await mysql.createConnection(conf.dbconfig);
        let [rows, fields] = await conn.execute(`SELECT * FROM \`tokens\` WHERE \`user\` = '${userid}'`);
        await conn.end();
        return rows;
      } catch (error) {
        throw error;
      }
    }

    function sortTokens(userTokensList) {
      let dateNowSTR = String(moment().format('YYYYMMDDhhmmss'));
      let dateNow = moment(f_lib.time.formatTime(dateNowSTR, moment().format('a')), "YYYYMMDDhhmmss");
      let activeTokens = [];
      let oldTokens = [];
      userTokensList.forEach(function (el, i) {
        // let pm = el.pm == 1 ? 'pm' : 'am';
        let dateToken = moment(el.date, "YYYYMMDDhhmmss");
        if (Math.abs(dateNow.diff(dateToken)) < conf.liveToken) {
          activeTokens.push(el);
        }
        else {
          oldTokens.push(el);
        }
      });
      return {
        activeTokens: activeTokens,
        oldTokens: oldTokens
      };
    }

    function isTokenActive(token, arr) {
      let result = arr.some((el, i) => {
        return el.token == token;
      })
      return result;
    }

    async function removeOldTokens(arr) {
      try {
        let conn = await mysql.createConnection(conf.dbconfig);
        arr.forEach(async (el, i) => {
          await conn.execute(`DELETE FROM \`tokens\` WHERE \`id\` = '${el.id}'`);
        });
        await conn.end();
        return true;
      } catch (error) {
        message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
        message.status = false;
        res.send(message);
        throw error;
      }
    }

    if (f_lib.objects.isSetData(userInfo)) {
      if (validate(userInfo)) {
        let userid = await f_lib.db.userByLogin(userInfo.login, message, res);
        if (userid) {
          message.status = true;
          let userTokensList = await getUserTokensList(userid[0].id);
          let sortTokensList = sortTokens(userTokensList);
          await removeOldTokens(sortTokensList.oldTokens);
          let isTokenActiveInfo = isTokenActive(userInfo.token, sortTokensList.activeTokens);
          return isTokenActiveInfo;
        }
      }
    }
    // console.log(data);
  } catch (e) {
    return false;
  }
}

async function checkUserType(login, types) {
  function validate(login, accesstypes) {
    let types = ['admin', 'moderator', 'user'];
    let resLogin = f_lib.strings.validateLoginsPasswords(login);
    let resType = accesstypes.some(el => {
      return types.includes(el);
    });
    return resLogin && resType;
  }

  async function getTypeById(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_types\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows[0].type;
    } catch (error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  if (!validate(login, types)) {
    return false;
  }

  let userInfo = await f_lib.db.userByLogin(login);
  let userType = await getTypeById(userInfo[0].type);

  if (types.includes(userType)) {
    return userType;
  }
  else {
    return false;
  }
}

async function sendUserTypeError(res) {
  res.send({
    status: false,
    content: 'Вы не можете осуществить данную операцию, так как тип вашей учётной записи не соответствует необходимому уровню доступа.'
  });
  return;
}

async function sendUserAccessError(res) {
  res.send({
    status: false,
    content: 'Вы не можете осуществить данную операцию, так как администратор не дал вам доступ для работы с этим рзаделом.'
  });
  return;
}

async function checkModerAccess(login, res) {
  function validate(login) {
    return f_lib.strings.validateDataNames(login);
  }

  async function getAdminOfModerator(userid) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_access\` WHERE \`user\` = '${userid}'`);
      if (rows.length > 0) {
        let [rows1, fields1] = await conn.execute(`SELECT * FROM \`users\` WHERE \`id\` = '${rows[0].admin}'`);
        await conn.end();
        return rows1[0].login;
      }
      await conn.end();
      return false;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData({ login: login })) {
    return false;
  }

  if (!validate(login)) {
    return false;
  }

  if (await checkUserType(login, ['admin', 'moderator', 'user']) != 'moderator') {
    return false;
  }

  let user = await f_lib.db.userByLogin(login);
  let adminOfModerator = await getAdminOfModerator(user[0].id);
  return adminOfModerator;
}

async function checkAccessToFunctional(login, functional, res) {
  function validate(login, fucntional) {
    let accessPunkts = f_lib.fullAccessList();

    return (
      f_lib.strings.validateLoginsPasswords(login) &&
      accessPunkts.includes(functional)
    );
  }

  async function checkFunctional(userid, functional) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      // let [rows, fields] = await conn.execute(`SELECT * FROM \`moderator_access\` WHERE \`moderator\` = '${userid}'`);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_access\` WHERE \`user\` = '${userid}'`);
      if (rows.length > 0) {
        let [rows1, fields] = await conn.execute(`SELECT * FROM \`moderator_access\` WHERE \`useraccess\` = '${rows[0].id}'`);
        await conn.end();
        if (rows1.length > 0) {
          return rows1[0][functional] == 1;
        }
      }
      return false;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!validate(login, functional)) {
    return false;
  }

  let userid = await f_lib.db.userByLogin(login);
  return await checkFunctional(userid[0].id, functional);
}

module.exports.checkToken = checkToken;
module.exports.checkUserType = checkUserType;
module.exports.sendUserTypeError = sendUserTypeError;
module.exports.sendUserAccessError = sendUserAccessError;
module.exports.checkModerAccess = checkModerAccess;
module.exports.checkAccessToFunctional = checkAccessToFunctional;