const mysql = require('mysql2/promise');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');
const fs = require('fs');
const shortid = require('shortid');

Array.prototype.unique = function () {
  let newArray = [];
  this.forEach(element => {
    if (!newArray.includes(element)) {
      newArray.push(element);
    }
  });
  return newArray;
};

async function createAssessment(data, res) {
  data.nameAssessment = f_lib.strings.changeDataNames(data.nameAssessment);

  function validate(data) {
    let resName = f_lib.strings.validateDataNames(data.nameAssessment);
    let resLogin = f_lib.strings.validateLoginsPasswords(data.userlogin);
    let resUsersInvites = true;
    try {
      resUsersInvites = data.usersInvites.every(invite => {
        return (
          f_lib.strings.validateDataNums(invite.user_from) &&
          f_lib.strings.validateDataNums(invite.user_to)
        );
      });
    } catch (error) {
      return false;
    }
    return resLogin && resName && resUsersInvites && data.usersInvites.length > 0;
  }

  async function isSetAssessment(data, adminID) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM assessments WHERE user=${adminID} AND name='${data.nameAssessment}'`);
      await conn.end();
      return rows.length == 0;
    }
    catch (error) {
      throw error;
    }
  }

  async function isSetUsers(data) {
    try {
      let result = data.usersInvites.reduce((previous, invite) => {
        return [...previous, ...[invite.user_from, invite.user_to]];
      }, []).unique();
      let check = [];
      let conn = await mysql.createConnection(conf.dbconfig);
      for (let i = 0; i < result.length; i++) {
        let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`id\` = '${result[i]}'`);
        check.push({ user: result[i], status: rows.length > 0 });
      }
      await conn.end();
      if (check.every(user => user.status > 0)) {
        return {
          status: true,
          content: true
        };
      }
      else {
        return {
          status: false,
          content: check.filter(user => user.status == 0).map(user => user.id)
        };
      }
    }
    catch (error) {
      throw error;
    }
  }

  async function isAllUsersCreatedByAdmin(data, adminID) {
    let usersId = data.usersInvites.reduce((previous, invite) => {
      return [...previous, ...[invite.user_from, invite.user_to]];
    }, []).unique();

    let typeAdmin = await f_lib.db.userTypeByName('admin');
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [users, users_fields] = await conn.execute(`SELECT id, type FROM users WHERE id IN(${usersId})`);
      if (!users.filter(user => user.id != adminID).map(type => type.type).unique().every(type => type != typeAdmin)) {
        return {
          status: false,
          content: 'Вы пытаетесь добавить пользователей, которые были созданы не вами.'
        };
      }
      let [rows, fields] = await conn.execute(`SELECT * FROM users_access WHERE user IN(${usersId})`);
      await conn.end();
      if (rows.every(user_access => user_access.admin == adminID)) {
        return {
          status: true,
          content: true
        }
      }
      else {
        return {
          status: false
        };
      }
    } catch (error) {
      throw error;
    }
  }

  async function usersWPList(data) {
    let usersId = data.usersInvites.reduce((previous, invite) => {
      return [...previous, ...[invite.user_from, invite.user_to]];
    }, []).unique();
    let resCheckWP = [];
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      for (let i = 0; i < usersId.length; i++) {
        let [rows, fields] = await conn.execute(`SELECT * FROM users_workpositions WHERE user = ${usersId[i]}`);
        let userInfo = await f_lib.db.userById(usersId[i]);
        if (rows.length == 0) {
          resCheckWP.push({
            user: userInfo[0],
            wp: null
          });
        }
        else {
          let [rows_wp, fields_wp] = await conn.execute(`SELECT * FROM workpositions WHERE id = ${rows[0].wp}`);
          resCheckWP.push({
            user: userInfo[0],
            wp: rows_wp[0]
          });
        }
      }
      await conn.end();
      return resCheckWP;
    } catch (error) {
      throw error;
    }
  }

  async function checkAssessmentListCompetencies(wplist) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM wpconnections WHERE wp IN (${wplist})`);
      let checkConnections = rows.map(connection => connection.wp).unique();
      let resultErrorInWPID = wpList.filter(wp => {
        return !checkConnections.includes(wp);
      });
      if (resultErrorInWPID.length > 0) {
        let [error_wp, fields1] = await conn.execute(`SELECT * FROM workpositions WHERE id IN (${resultErrorInWPID})`);
        await conn.end();
        return {
          status: false,
          content: error_wp.map(errwp => {
            return `У должности \"${errwp.name}\" отсутствуют компетенции. Что бы создать мероприятие, добавьте компетенции всем должностям, привязанным к сотрудникам, которые принимают участие в мероприятии.`
          }).join('\n')
        };
      }
      else {
        let competenciesIDList = rows.map(row => row.competence).unique();
        let [competenciesList, competenciesFields] = await conn.execute(`SELECT * FROM competencies WHERE id IN (${competenciesIDList})`);
        await conn.end();
        return {
          status: true,
          content: competenciesList
        };
      }
    } catch (error) {
      throw error;
    }
  }

  async function checkAssessmentListCriteria(competencieslist) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM criteria WHERE competence IN (${competencieslist.map(competence => competence.id)})`);
      let competenciesCollection = rows.map(row => row.competence).unique();
      let resultErrorInCompetenceID = competencieslist.map(competence => competence.id).filter(id => {
        return !competenciesCollection.includes(id);
      });
      if (resultErrorInCompetenceID.length > 0) {
        let [error_competence, fields1] = await conn.execute(`SELECT * FROM competencies WHERE id IN (${resultErrorInCompetenceID})`);
        await conn.end();
        return {
          status: false,
          content: error_competence.map(errcp => {
            return `У компетенции \"${errcp.name}\" отсутствуют критерии оценки. Что бы создать мероприятие, добавьте критерии оценки всем компетенциям, привязанным к оценочным листам, используемым в мероприятии.`
          }).join('\n')
        };
      }
      else {
        let criteriaIDList = rows.map(row => row.id);
        let [criteriaList, criteriaFields] = await conn.execute(`SELECT * FROM criteria WHERE id IN (${criteriaIDList})`);
        await conn.end();
        return {
          status: true,
          content: criteriaList
        };
      }
    } catch (error) {
      throw error;
    }
  }

  async function checkAssessmentListSigns(criterialist, competencieslist) {
    let resCheck = [];
    let resError = [];
    let counterErrors = 0;
    let conn = await mysql.createConnection(conf.dbconfig);
    for (let i = 0; i < criterialist.length; i++) {
      let [rows, fields] = await conn.execute(`SELECT * FROM signs WHERE criterion = (${criterialist[i].id}) ORDER BY weight DESC`);
      if (rows.length < 2) {
        counterErrors++;
        resError.push(`У критерия \"${criterialist[i].text}\", относящегося к компетенции \"${competencieslist.filter(competence => competence.id == criterialist[i].competence)[0].name}\" недостаточно признаков. Для начала мероприятия у всех критериев должно быть хотя бы два признака.`);
      }
      else {
        resCheck = [...resCheck, ...rows];
      }
    }
    await conn.end();
    if (counterErrors > 0) {
      return {
        status: false,
        content: resError.join('\n')
      };
    }
    else {
      return {
        status: true,
        content: resCheck
      };
    }
  }

  async function getWPCompetenciesConnections(wplist) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM wpconnections WHERE wp IN(${wplist.map(wp => wp.id)})`);
      await conn.end();
      return rows;
    } catch (error) {
      throw error;
    }
    return true;
  }

  async function get_unique_wp_list(data) {
    let usersId = data.usersInvites.reduce((previous, invite) => {
      return [...previous, ...[invite.user_from, invite.user_to]];
    }, []).unique();
    let resCheckWP = [];
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      for (let i = 0; i < usersId.length; i++) {
        let [rows, fields] = await conn.execute(`SELECT * FROM users_workpositions WHERE user = ${usersId[i]}`);
        let userInfo = await f_lib.db.userById(usersId[i]);
        if (rows.length == 0) {
          resCheckWP.push({
            user: userInfo[0],
            wp: null
          });
        }
        else {
          let [rows_wp, fields_wp] = await conn.execute(`SELECT * FROM workpositions WHERE id = ${rows[0].wp}`);
          if (resCheckWP.every(wp_info => wp_info.id != rows_wp[0].id)) {
            resCheckWP.push(rows_wp[0]);
          }
        }
      }
      await conn.end();
      return resCheckWP;
    } catch (error) {
      throw error;
    }
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  let adminID = await f_lib.db.userByLogin(data.userlogin);

  if (!await isSetAssessment(data, adminID[0].id)) {
    res.send({
      status: false,
      content: 'Мероприятие с таким названием уже существует.'
    });
    return false;
  }

  let resIsSetUsers = await isSetUsers(data);

  if (!resIsSetUsers.status) {
    res.send({
      status: false,
      content: 'Не все польователи из списка существуют. Вернитесь во вкладку \"Укажите, кто кого должен оценить\", в меню \"Операции\" выберите пункт \"Удалить неактивных пользователей\" и повторите попытку создания мероприятия.'
    });
    return false;
  }

  if (!await isAllUsersCreatedByAdmin(data, adminID[0].id)) {
    res.send({
      status: false,
      content: 'Вы пытаетесь добавить в мероприятие пользователей, которые были созданы не вами.'
    });
    return false;
  }

  let checkwp = await usersWPList(data);

  let allUsersHaveWP = checkwp.filter(rowCheck => {
    return rowCheck.wp == null;
  }).map(info => {
    return `У пользователя \"${info.user.surename} ${info.user.name} ${info.user.patronymic} (${info.user.login})\" отсутствует должность. Укажите должность пользователя, что бы он мог принять участие в мероприятии.\"`;
  });

  if (allUsersHaveWP.length > 0) {
    res.send({
      status: false,
      content: allUsersHaveWP.join('\n')
    });
    return false;
  }

  if ((checkwp.map(element => element.wp.user).unique().length != 1) || (checkwp.map(element => element.wp.user).unique()[0] != adminID[0].id)) {
    res.send({
      status: false,
      content: 'Невозможно создать мероприятие, так как некоторые должности, присвоенные пользователям, созданы разными пользователями.'
    });
    return false;
  }

  let wpList = checkwp.filter(rowCheck => {
    return rowCheck.wp != null;
  }).map(element => element.wp.id).unique();

  let checkCompetencies = await checkAssessmentListCompetencies(wpList);

  if (!checkCompetencies.status) {
    res.send({
      status: false,
      content: checkCompetencies.content
    });
    return false;
  }

  if ((checkCompetencies.content.map(element => element.user).unique().length != 1) || (checkCompetencies.content.map(element => element.user).unique()[0] != adminID[0].id)) {
    res.send({
      status: false,
      content: 'Невозможно создать мероприятие, так как некоторые компетенции, присвоенные должностям, созданы разными пользователями.'
    });
    return false;
  }


  let resCheckCriteria = await checkAssessmentListCriteria(checkCompetencies.content);

  if (!resCheckCriteria.status) {
    res.send({
      status: false,
      content: resCheckCriteria.content
    });
    return false;
  }

  let resCheckSigns = await checkAssessmentListSigns(resCheckCriteria.content, checkCompetencies.content);


  if (!resCheckSigns.status) {
    res.send({
      status: false,
      content: resCheckSigns.content
    });
    return false;
  }

  let unique_wp_list = await get_unique_wp_list(data);

  const WP_LIST = unique_wp_list;
  const COMPETENCIES_LIST = checkCompetencies.content;
  const WP_COMPETENCIES_CONNECTIONS = await getWPCompetenciesConnections(WP_LIST);
  const CRITERIA_LIST = resCheckCriteria.content;
  const SIGNS_LIST = resCheckSigns.content;


  const ARRAY_ASSESSMENT_LISTS_COMPETENCIES = COMPETENCIES_LIST.map(competence => {
    return {
      COMPETENCE: competence,
      CRITERIA: CRITERIA_LIST.filter(criteria => {
        return criteria.competence == competence.id;
      }).map(criterion => {
        return {
          CRITERION: criterion,
          SIGNS: SIGNS_LIST.filter(sign => {
            return sign.criterion == criterion.id;
          }).map(sign => {
            return {
              ...sign,
              selected: false
            };
          })
        }
      })
    };
  });

  const ARRAY_ASSESSMENT_LISTS_WP = WP_LIST.map(wp => {
    return {
      WP: wp,
      COMPETENCIES: ARRAY_ASSESSMENT_LISTS_COMPETENCIES.filter(competence_info => {
        return WP_COMPETENCIES_CONNECTIONS.filter(connection => {
          return connection.wp == wp.id
        }).map(element => element.competence).includes(competence_info.COMPETENCE.id);
      })
    }
  });

  let assessmentFormsCollection = {};

  for (let i = 0; i < ARRAY_ASSESSMENT_LISTS_WP.length; i++) {
    let filename = shortid.generate();
    assessmentFormsCollection[ARRAY_ASSESSMENT_LISTS_WP[i].WP.id] = filename;
    var fileContent = JSON.stringify(ARRAY_ASSESSMENT_LISTS_WP[i]);
    var filepath = `users_file_system/${adminID[0].dir}/assessmentforms/${filename}.json`;

    if (!fs.existsSync(`users_file_system/${adminID[0].dir}`)) {
      fs.mkdirSync(`users_file_system/${adminID[0].dir}`);
    }

    if (!fs.existsSync(`users_file_system/${adminID[0].dir}/assessmentforms`)) {
      fs.mkdirSync(`users_file_system/${adminID[0].dir}/assessmentforms`);
    }

    fs.writeFileSync(filepath, fileContent, (err) => {
      if (err) throw err;
    });
  }
  let checkwpList = checkwp.map(checkRow => {
    return {
      user: checkRow.user.id,
      wp: checkRow.wp.id
    };
  })

  let assessmentInvitesData = data.usersInvites.map(element => {
    return {
      ...element,
      form: assessmentFormsCollection[checkwpList.filter(el => el.user == element.user_to)[0].wp]
    };
  });

  try {
    let conn = await mysql.createConnection(conf.dbconfig);
    let result = await conn.execute(`INSERT INTO assessments (\`user\`,\`name\`) VALUES ('${adminID[0].id}', '${data.nameAssessment}')`);
    for (let i = 0; i < assessmentInvitesData.length; i++) {
      await conn.execute(`INSERT INTO assessments_connections (assessment, user_from, user_to, assessments_form) VALUES (${result[0].insertId},${assessmentInvitesData[i].user_from},${assessmentInvitesData[i].user_to},'${assessmentInvitesData[i].form}')`);
    }
    await conn.end();
  } catch (error) {
    res.send({
      status: false,
      content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
    });
    throw error;
  }

  res.send({
    status: true,
    content: true
  });

  return true;
}

async function getuserInvites(data, res) {
  function validate(data) {
    let resLogin = f_lib.strings.validateLoginsPasswords(data.userlogin);
    return resLogin;
  }

  async function getUserInvites(userid) {
    try {
      let result = [];
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM assessments_connections WHERE user_from = '${userid}'`);
      for (let i = 0; i < rows.length; i++) {
        let user = await f_lib.db.userById(rows[i].user_to);
        result.push({
          id: rows[i].id,
          user_to: {
            id: user[0].id,
            login: user[0].login,
            name: user[0].name,
            surename: user[0].surename,
            patronymic: user[0].patronymic
          }
        });
      }
      await conn.end();
      return result;
    } catch (error) {
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Ничего не найдено.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Ничего не найдено.'
    });
    return false;
  }

  let userInfo = await f_lib.db.userByLogin(data.userlogin);

  let result = await getUserInvites(userInfo[0].id);

  res.send({
    status: true,
    content: result
  });
  return true;
}

async function getformbyinvite(data, res) {
  function validate(data) {
    let resLogin = f_lib.strings.validateLoginsPasswords(data.userlogin);
    let resInvite = f_lib.strings.validateLoginsPasswords(data.inviteId);
    return resLogin && resInvite;
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  let userInfo = await f_lib.db.userByLogin(data.userlogin);

  try {
    let conn = await mysql.createConnection(conf.dbconfig);
    let [rows, fields] = await conn.execute(`SELECT * FROM assessments_connections WHERE id = ${data.inviteId} AND user_from = '${userInfo[0].id}'`);
    let [rows1, fields1] = await conn.execute(`SELECT * FROM users WHERE id IN (
      SELECT user FROM assessments WHERE id IN (
        SELECT assessment FROM assessments_connections WHERE id = ${data.inviteId}
      )
    )`);
    await conn.end();
    if (rows.length == 0) {
      res.send({
        status: false,
        content: 'Ничего не найдено.'
      });
      return false;
    }

    let dirName = `users_file_system/${rows1[0].dir}/assessmentforms/${rows[0].assessments_form}.json`;
    const file = fs.readFileSync(dirName, "utf8");

    res.send({
      status: true,
      content: JSON.parse(file)
    });

  } catch (error) {
    throw error;
  }
}

async function setresults(data, res) {

  let fileName_p1 = null;
  let fileName_p2 = null;

  let user_from = null;
  let user_to = null;
  let assessmentid = null;
  let resultdir = shortid.generate();

  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.invite)
    )
  }

  async function getForm(data) {
    let userInfo = await f_lib.db.userByLogin(data.userlogin);

    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM assessments_connections WHERE id = ${data.invite} AND user_from = '${userInfo[0].id}'`);
      let [rows1, fields1] = await conn.execute(`SELECT * FROM users WHERE id IN (
        SELECT user FROM assessments WHERE id IN (
          SELECT assessment FROM assessments_connections WHERE id = ${data.invite}
        )
      )`);

      let [get_user_from, uf_fields] = await conn.execute(`SELECT id, login, name, surename, patronymic FROM users WHERE id = ${rows[0].user_from}`);
      let [get_user_to, ut_fields] = await conn.execute(`SELECT id, login, name, surename, patronymic FROM users WHERE id = ${rows[0].user_to}`);

      user_from = get_user_from[0];
      user_to = get_user_to[0];
      assessmentid = rows[0].assessment;

      await conn.end();
      if (rows.length == 0) {
        res.send({
          status: false,
          content: 'Ничего не найдено.'
        });
        return false;
      }

      let dirName = `users_file_system/${rows1[0].dir}/assessmentforms/${rows[0].assessments_form}.json`;
      fileName_p1 = `users_file_system/${rows1[0].dir}/`;
      fileName_p2 = `/${rows[0].assessments_form}`;
      const file = fs.readFileSync(dirName, "utf8");

      return JSON.parse(file);

    } catch (error) {
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: false
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: false
    });
    return false;
  }

  let exampleForm = await getForm(data);

  let exampleWP = JSON.stringify(exampleForm.WP);

  let wp = JSON.stringify(data.results.WP);

  if (wp != exampleWP) {
    res.send({
      status: false,
      content: 'Название должности не совпадает.'
    });
    return false;
  }

  let exampleCompetencies = exampleForm.COMPETENCIES.map(competence_info => {
    return competence_info.COMPETENCE;
  });

  let competencies = data.results.COMPETENCIES.map(competence_info => {
    return competence_info.COMPETENCE;
  });

  if (JSON.stringify(exampleCompetencies) != JSON.stringify(competencies)) {
    res.send({
      status: false,
      content: 'Компетенции указаны некорректно.'
    });
    return false;
  }

  let exampleCriteria = data.results.COMPETENCIES.map(competence_info => {
    return competence_info.CRITERIA.map(criterion_info => {
      return criterion_info.CRITERION;
    })
  });

  let criteria = exampleForm.COMPETENCIES.map(competence_info => {
    return competence_info.CRITERIA.map(criterion_info => {
      return criterion_info.CRITERION;
    })
  });

  if (JSON.stringify(exampleCriteria) != JSON.stringify(criteria)) {
    res.send({
      status: false,
      content: 'Критерии указаны некорректно.'
    });
    return false;
  }

  // let exampleSigns = data.results.COMPETENCIES.map(competence_info => {
  //   return competence_info.CRITERIA.map(criterion_info => {
  //     return criterion_info.CRITERION;
  //   })
  // });

  let selectedSigns = [];

  let exampleSigns = data.results.COMPETENCIES.map(competence_info => {
    return competence_info.CRITERIA.map(criterion_info => {
      return criterion_info.SIGNS.map(sign_info => {

        if (sign_info.selected == true) {
          selectedSigns.push(sign_info.id);
        }

        return {
          id: sign_info.id,
          criterion: sign_info.criterion,
          text: sign_info.text,
          weight: sign_info.weight
        };
      });
    })
  });

  let signs = exampleForm.COMPETENCIES.map(competence_info => {
    return competence_info.CRITERIA.map(criterion_info => {
      return criterion_info.SIGNS.map(sign_info => {
        return {
          id: sign_info.id,
          criterion: sign_info.criterion,
          text: sign_info.text,
          weight: sign_info.weight
        };
      });
    })
  });

  if (JSON.stringify(exampleSigns) != JSON.stringify(signs)) {
    res.send({
      status: false,
      content: 'Признаки указаны некорректно.'
    });
    return false;
  }

  exampleForm.USER_FROM = user_from;
  exampleForm.USER_TO = user_to;

  exampleForm.COMPETENCIES.map(competence_info => {
    return competence_info.CRITERIA.map(criterion_info => {
      return criterion_info.SIGNS.map(sign_info => {
        if (selectedSigns.includes(sign_info.id)) {
          if (criterion_info.CRITERION.typecheckbox == 1) {
            sign_info.selected = true;
          }
          else {
            if (criterion_info.SIGNS.every(sign_info => {
              return sign_info.selected == false;
            })) {
              sign_info.selected = true;
            }
          }
        }
      });
    })
  });

  let hrefSave = `${fileName_p1}/results/${resultdir}.json`;

  if (!fs.existsSync(`${fileName_p1}`)) {
    fs.mkdirSync(`${fileName_p1}`);
  }

  if (!fs.existsSync(`${fileName_p1}/results`)) {
    fs.mkdirSync(`${fileName_p1}/results`);
  }

  fs.writeFileSync(hrefSave, JSON.stringify(exampleForm), (err) => {
    if (err) throw err;
  });

  try {
    let conn = await mysql.createConnection(conf.dbconfig);
    let [rows, fields] = await conn.execute(`SELECT * FROM assessments_connections WHERE assessments_form = '${fileName_p2}'`);
    await conn.end();
    if (rows.length == 1) {
      fs.unlinkSync(`${fileName_p1}assessmentforms${fileName_p2}.json`);
    }
  } catch (error) {
    throw error;
  }

  try {
    let conn = await mysql.createConnection(conf.dbconfig);
    await conn.execute(`DELETE FROM assessments_connections WHERE id = ${data.invite}`);
    await conn.execute(`INSERT INTO results (assessment, content) VALUES (${assessmentid},'${resultdir}')`);
    await conn.end();
  } catch (error) {
    throw error;
  }

  res.send({
    status: true,
    content: true
  });
}

async function getAssessmentsList(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  data.nameAssessment = f_lib.strings.changeDataNames(data.nameAssessment);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let wp = f_lib.strings.validateDataNames(data.nameAssessment) || (data.nameAssessment == '');
    let searchByAlphabet = data.sortabc == 'true' || data.sortabc == 'false';
    return login && wp && searchByAlphabet;
  }

  async function searchAssessmentsinDB(name, login, sortabc) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`assessments\` WHERE \`user\` = '${user[0].id}' AND \`name\` LIKE '%${name}%' ${sortabc == 'true' ? ' ORDER BY \`name\` ASC' : ''}`);
      await conn.end();
      return rows;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }
  let resultSearch = [];
  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      resultSearch = await searchAssessmentsinDB(data.nameAssessment, data.login, data.sortabc);
    }
    else {
      counterErrors = 1;
    }
  }
  else {
    counterErrors = 1;
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resultSearch
    });
  }
  else {
    res.send({
      status: false,
      content: resultSearch
    });
  }
}

async function renameAssessment(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let resrename = null;

  data.nameassessment = f_lib.strings.changeDataNames(data.nameassessment);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let assessment = f_lib.strings.validateDataNames(data.nameassessment);
    let id = f_lib.strings.validateDataNums(data.id);
    return login && assessment && id;
  }

  async function isassessmentCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`assessments\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetassessmentinDB(id, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`assessments\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.id == id
      }).length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetassessment(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`assessments\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function renameassessmentinDB(id, name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`UPDATE \`assessments\` SET \`name\`='${name}' WHERE \`id\` = '${id}' AND \`user\`= '${user[0].id}'`);
      let getNewassessmentById = await conn.execute(`SELECT * FROM \`assessments\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return getNewassessmentById[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetassessmentinDB(data.id, data.login)) {
        if (await isSetassessment(data.nameassessment, data.login)) {
          if (await isassessmentCreatedByThisUser(data.id, data.login)) {
            resrename = await renameassessmentinDB(data.id, data.nameassessment, data.login);
          }
          else {
            counterErrors = 1;
            errorsList.push('У вас нет доступа для редактирования этих данных.');
          }
        }
        else {
          counterErrors = 1;
          errorsList.push('Мероприятие с таким названием уже существует.');
        }
      }
      else {
        counterErrors = 1;
        errorsList.push('Редактируемое мероприятие не найдена.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название мероприятия может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resrename
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function deleteassessment(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  function validate(data) {
    let id = f_lib.strings.validateDataNums(data.id);
    let login = f_lib.strings.validateLoginsPasswords(data.login);
    return id && login;
  }

  async function isAssessmentCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`assessments\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteAssessment(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [invites, invites_fields] = await conn.execute(`SELECT assessments_form FROM assessments_connections WHERE assessment=${id}`);
      let [results, results_fields] = await conn.execute(`SELECT content FROM results WHERE assessment=${id}`);
      let [user_info, user_info_fields] = await conn.execute(`SELECT * FROM users WHERE id IN (
        SELECT user FROM assessments WHERE id=${id}
      )`);
      invites.map(el => el.assessments_form).unique().forEach(invite => {
        fs.unlinkSync(`users_file_system/${user_info[0].dir}/assessmentforms/${invite}.json`);
      });
      results.map(el => el.content).unique().forEach(result => {
        fs.unlinkSync(`users_file_system/${user_info[0].dir}/results/${result}.json`);
      });
      await conn.execute(`DELETE FROM \`assessments\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isAssessmentCreatedByThisUser(data.id, data.login)) {
        await deleteAssessment(data.id);
      }
      else {
        counterErrors = 1;
        errorsList.push('У вас нет доступа для редактирования этих данных.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Данные указаны некорректно.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Данные не указаны.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: 'Данные удалены.'
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function getassessmentresults(data, res) {
  function validate(data) {
    let id = f_lib.strings.validateDataNums(data.assessment);
    let login = f_lib.strings.validateLoginsPasswords(data.login);
    return id && login;
  }

  async function isAssessmentCreatedByThisUser(data) {
    let user = await f_lib.db.userByLogin(data.login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM assessments WHERE id = '${data.assessment}' AND user = ${user[0].id}`);
      await conn.end();      
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: false
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: false
    });
    return false;
  }

  if(!await isAssessmentCreatedByThisUser(data)){
    res.send({
      status: false,
      content: false
    });
    return false;
  }
  
  try {
    let conn = await mysql.createConnection(conf.dbconfig);
    let user = await f_lib.db.userByLogin(data.login);
    let [rows, fields] = await conn.execute(`SELECT content FROM results WHERE assessment = '${data.assessment}'`);
    let resultsArray = rows.map(content => content.content).map(content => {
      return JSON.parse(fs.readFileSync(`users_file_system/${user[0].dir}/results/${content}.json`, 'utf8'));
    });

    res.send({
      status: true,
      content: resultsArray
    });
    
    await conn.end();
  } catch (error) {
    res.send({
      status: false,
      content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
    });
    throw error;
  }
}

module.exports.createAssessment = createAssessment;
module.exports.getuserInvites = getuserInvites;
module.exports.getformbyinvite = getformbyinvite;
module.exports.setresults = setresults;
module.exports.getAssessmentsList = getAssessmentsList;
module.exports.renameAssessment = renameAssessment;
module.exports.deleteassessment = deleteassessment;
module.exports.getassessmentresults = getassessmentresults;