const development = {
  host: 'localhost',
  user: 'root',
  password: '11111111',
  database: 'hrapp'
};

const production = {
  host: '142.93.141.170',
  user: 'YLCG6nv38Aja55qT',
  password: '5w4JALpCrLtCrfX9',
  database: 'hrapp',
}

const conf = {
  development,
  production
}

console.log(process.env.NODE_ENV)

module.exports.dbconfig = {
  host: conf[process.env.NODE_ENV].host,
  user: conf[process.env.NODE_ENV].user,
  password: conf[process.env.NODE_ENV].password,
  database: conf[process.env.NODE_ENV].database
};
module.exports.validate = {
  names: /^[а-яА-ЯёЁіІїЇєЄa-zA-Z0-9_-]+$/,
  passwords: /^[A-Za-z0-9_]+$/,
  nums: /^[0-9]+$/,
  dataNames: /^[а-яА-ЯёЁіІїЇєЄa-zA-Z0-9_-\s]+$/
};
module.exports.liveToken = 1000 * 60 * 60 * 96;
