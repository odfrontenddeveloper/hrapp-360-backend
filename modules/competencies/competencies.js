const mysql = require('mysql2/promise');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');
const fs = require('fs');

async function addCompetence(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let newCompetenceRow = null;

  data.nameCompetence = f_lib.strings.changeDataNames(data.nameCompetence);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let competence = f_lib.strings.validateDataNames(data.nameCompetence);
    return login && competence;
  }

  async function isSetCompetence(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addWPtoDB(name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = await conn.execute(`INSERT INTO \`competencies\` (\`user\`, \`name\`) VALUES ('${user[0].id}', '${name}');`);
      let getNewCompetenceById = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${result[0].insertId}'`);
      await conn.end();
      return getNewCompetenceById[0][0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetCompetence(data.nameCompetence, data.login)) {
        newCompetenceRow = await addWPtoDB(data.nameCompetence, data.login);
      }
      else {
        counterErrors = 1;
        errorsList.push('Компетенция с таким названием уже существует.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название компетенции может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: newCompetenceRow
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function searchCompetence(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  data.nameCompetence = f_lib.strings.changeDataNames(data.nameCompetence);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let competence = f_lib.strings.validateDataNames(data.nameCompetence) || (data.nameCompetence == '');
    let searchByAlphabet = data.sortabc == 'true' || data.sortabc == 'false';
    return login && competence && searchByAlphabet;
  }

  async function searchCompetenceinDB(name, login, sortabc) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`user\` = '${user[0].id}' AND \`name\` LIKE '%${name}%' ${sortabc == 'true' ? ' ORDER BY \`name\` ASC' : ''}`);
      await conn.end();
      return rows;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }
  let resultSearch = [];
  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      resultSearch = await searchCompetenceinDB(data.nameCompetence, data.login, data.sortabc);
    }
    else {
      counterErrors = 1;
    }
  }
  else {
    counterErrors = 1;
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resultSearch
    });
  }
  else {
    res.send({
      status: false,
      content: resultSearch
    });
  }
}

async function deleteCompetence(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  function validate(data) {
    let id = f_lib.strings.validateDataNums(data.id);
    let login = f_lib.strings.validateLoginsPasswords(data.login);
    return id && login;
  }

  async function isCompetenceCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteCompetence(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM \`competencies\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isCompetenceCreatedByThisUser(data.id, data.login)) {
        await deleteCompetence(data.id);
      }
      else {
        counterErrors = 1;
        errorsList.push('У вас нет доступа для редактирования этих данных.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Данные указаны некорректно.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Данные не указаны.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: 'Данные удалены'
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function renameCompetence(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let resrename = null;

  data.nameCompetence = f_lib.strings.changeDataNames(data.nameCompetence);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let competence = f_lib.strings.validateDataNames(data.nameCompetence);
    let id = f_lib.strings.validateDataNums(data.id);
    return login && competence && id;
  }
  /* OK! */

  async function isCompetenceCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetCompetenceinDB(id, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.id == id
      }).length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }
  /* OK! */

  async function isSetCompetence(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function renameCompetenceinDB(id, name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`UPDATE \`competencies\` SET \`name\`='${name}' WHERE \`id\` = '${id}' AND \`user\`= '${user[0].id}'`);
      let getNewWPById = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return getNewWPById[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetCompetenceinDB(data.id, data.login)) {
        if (await isSetCompetence(data.nameCompetence, data.login)) {
          if (await isCompetenceCreatedByThisUser(data.id, data.login)) {
            resrename = await renameCompetenceinDB(data.id, data.nameCompetence, data.login);
          }
          else {
            counterErrors = 1;
            errorsList.push('У вас нет доступа для редактирования этих данных.');
          }
        }
        else {
          counterErrors = 1;
          errorsList.push('Компетенция с таким названием уже существует.');
        }
      }
      else {
        counterErrors = 1;
        errorsList.push('Редактируемая компетенция не найдена.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название компетенции может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resrename
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function connectCompetenceToWP(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.login) &&
      f_lib.strings.validateDataNums(data.idwp) &&
      f_lib.strings.validateDataNums(data.idcompetence) &&
      data.connect === true
    );
  }

  async function isCompetenceConnectedToWP(login, wp, competence, res) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getWP = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${wp}' AND \`user\` = '${user[0].id}'`);
      let getCP = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${competence}' AND \`user\` = '${user[0].id}'`);
      let isSetConnect = await conn.execute(`SELECT * FROM \`wpconnections\` WHERE \`wp\` = '${wp}' AND \`competence\` = '${competence}'`);

      if (getWP[0][0].user != getCP[0][0].user) {
        res.send({
          status: false,
          content: 'Должность и компетенция созданы разными пользователями.'
        });
        return false;
      }

      if (getWP[0][0].user != user[0].id || getCP[0][0].user != user[0].id) {
        res.send({
          status: false,
          content: 'У вас нет прав для редактирования этих данных.'
        });
        return false;
      }

      if (isSetConnect[0].length != 0) {
        res.send({
          status: false,
          content: 'Данная связка уже существует.'
        });
        return false;
      }

      await conn.end();

      return true;

    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addConnect(wp, competence) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = await conn.execute(`INSERT INTO \`wpconnections\` (\`wp\`, \`competence\`) VALUES ('${wp}', '${competence}');`);
      let getNewConnectById = await conn.execute(`SELECT * FROM \`wpconnections\` WHERE \`id\` = '${result[0].insertId}'`);
      await conn.end();
      return getNewConnectById[0][0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }


  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные не указаны.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны неверно.'
    });
    return false;
  }

  if (!await isCompetenceConnectedToWP(data.login, data.idwp, data.idcompetence, res)) {
    return false;
  }

  let resAddConnect = await addConnect(data.idwp, data.idcompetence);

  res.send({
    status: true,
    content: resAddConnect
  });

  return true;
}

async function disconnectCompetenceFromWP(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.login) &&
      f_lib.strings.validateDataNums(data.idwp) &&
      f_lib.strings.validateDataNums(data.idcompetence) &&
      data.connect === false
    );
  }

  async function isCompetenceConnectedToWP(login, wp, competence, res) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getWP = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${wp}' AND \`user\` = '${user[0].id}'`);
      let getCP = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${competence}' AND \`user\` = '${user[0].id}'`);
      let isSetConnect = await conn.execute(`SELECT * FROM \`wpconnections\` WHERE \`wp\` = '${wp}' AND \`competence\` = '${competence}'`);

      if (getWP[0][0].user != getCP[0][0].user) {
        res.send({
          status: false,
          content: 'Должность и компетенция созданы разными пользователями.'
        });
        return false;
      }

      if (getWP[0][0].user != user[0].id || getCP[0][0].user != user[0].id) {
        res.send({
          status: false,
          content: 'У вас нет прав для редактирования этих данных.'
        });
        return false;
      }

      if (isSetConnect[0].length == 0) {
        res.send({
          status: false,
          content: 'Данной связки не существует.'
        });
        return false;
      }

      await conn.end();

      return true;

    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteConnect(wp, competence) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM \`wpconnections\` WHERE \`wp\` = '${wp}' AND \`competence\` = '${competence}'`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }


  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные не указаны.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны неверно.'
    });
    return false;
  }

  if (!await isCompetenceConnectedToWP(data.login, data.idwp, data.idcompetence, res)) {
    return false;
  }

  let resDelete = await deleteConnect(data.idwp, data.idcompetence);

  res.send({
    status: true,
    content: 'Связка удалена.'
  });

  return true;
}

async function searchConnects(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateDataNums(data.wp) && f_lib.strings.validateLoginsPasswords(data.login)
    );
  }

  async function isCompetenceCreatedByThisUser(data) {
    let user = await f_lib.db.userByLogin(data.login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getWP = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${data.wp}' AND \`user\`='${user[0].id}'`);
      await conn.end();

      return getWP[0].length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function getFullConnectsList(data) {
    let user = await f_lib.db.userByLogin(data.login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let getWP = await conn.execute(`SELECT * FROM \`wpconnections\` WHERE \`wp\` = '${data.wp}'`);
      await conn.end();
      return getWP[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Данные не указаны.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны неверно.'
    });
    return false;
  }

  if (!await isCompetenceCreatedByThisUser(data)) {
    res.send({
      status: false,
      content: 'У вас нет прав для получения этой информации.'
    });
    return false;
  }

  let fullList = await getFullConnectsList(data);

  res.send({
    status: true,
    content: fullList
  });
  return true;
}

module.exports.addCompetence = addCompetence;
module.exports.searchCompetence = searchCompetence;
module.exports.deleteCompetence = deleteCompetence;
module.exports.renameCompetence = renameCompetence;
module.exports.connectCompetenceToWP = connectCompetenceToWP;
module.exports.disconnectCompetenceFromWP = disconnectCompetenceFromWP;
module.exports.searchConnects = searchConnects;