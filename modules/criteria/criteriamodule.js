const mysql = require('mysql2/promise');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');
const controlAuth = require('../controlauth/controlAuth.js');
const fs = require('fs');

async function createCriteria(data, res) {

  data.criterion = f_lib.strings.changeDataNames(data.criterion);
  data.signs = data.signs.map(element => {
    return {
      name: f_lib.strings.changeDataNames(element.name),
      value: element.value
    };
  });

  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.competence) &&
      f_lib.strings.validateDataNames(data.criterion) &&
      (data.criterionTypeCheckbox === true || data.criterionTypeCheckbox === false) &&
      data.signs.every(element => {
        return (f_lib.strings.validateDataNames(element.name) &&
          f_lib.strings.validateDataNums(element.value));
      })
    );
  }

  async function isCompetenceCreatedByThisUser(id, user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${id}' AND \`user\` = ${user}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetCriterionInDB(competence, criterionName) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`criteria\` WHERE \`text\` = '${criterionName}' AND \`competence\` = ${competence}`);
      await conn.end();
      return rows.length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addCriterionToDB(data) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let resAddSignsID = [];
      let resAddCriterion = await conn.execute(`INSERT INTO criteria (competence, text, typecheckbox) VALUES ('${data.competence}', '${data.criterion}', '${data.criterionTypeCheckbox ? 1 : 0}')`);
      for (let i = 0; i < data.signs.length; i++) {
        let addSign = await conn.execute(`INSERT INTO signs (criterion, text, weight) VALUES ('${resAddCriterion[0].insertId}', '${data.signs[i].name}', '${data.signs[i].value}')`);
        resAddSignsID.push(addSign[0].insertId);
      }

      let [criterion, fields] = await conn.execute(`SELECT * FROM \`criteria\` WHERE \`id\` = '${resAddCriterion[0].insertId}'`);
      let [signs, fields1] = await conn.execute(`SELECT * FROM \`signs\` WHERE \`criterion\` = '${resAddCriterion[0].insertId}'`);
      await conn.end();
      return {
        criterion: criterion[0],
        signs: signs
      };
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  if (data.signs.length < 2) {
    res.send({
      status: false,
      content: 'Количество признаков не может быть меньше 2.'
    });
    return false;
  }

  if (!data.signs.every(element => element.value <= 100)) {
    res.send({
      status: false,
      content: 'Максимальное значение шкалы признака - 100.'
    });
    return false;
  }

  if (data.criterionTypeCheckbox === true) {
    let sumSigns = 0;
    data.signs.map(el => {
      sumSigns += el.value;
    });

    if (sumSigns != 100) {
      res.send({
        status: false,
        content: 'Для критерия с несколькими вариантами ответа суммарное значение по шкалам всех признаков должно составлять ровно 100.'
      });
      return false;
    }
  }

  let userInfo = await f_lib.db.userByLogin(data.userlogin);

  if (!await isCompetenceCreatedByThisUser(data.competence, userInfo[0].id)) {
    res.send({
      status: false,
      content: 'Указанная компетенция создана не вами.'
    });
    return false;
  }

  if (!await isSetCriterionInDB(data.competence, data.criterion)) {
    res.send({
      status: false,
      content: 'Такой критерий для данной компетенции уже есть.'
    });
    return false;
  }

  let rescreate = await addCriterionToDB(data);

  res.send({
    status: true,
    content: rescreate
  });
}

async function searchCriteria(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      (f_lib.strings.validateDataNums(data.competence) || data.competence == '')
    );
  }

  async function isCompetenceCreatedByThisUser(id, user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${id}' AND \`user\` = ${user}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function getCriteriaList(competence, user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [criteriaList, fields] = await conn.execute(`SELECT * FROM criteria WHERE competence = ${competence} AND competence IN (
        SELECT id FROM competencies WHERE user = ${user}
      )`);
      let [signslist, fields1] = await conn.execute(`SELECT * FROM signs WHERE criterion IN (
        SELECT id FROM criteria WHERE competence = ${competence}
      ) ORDER BY weight DESC`);

      let listForUser = criteriaList.map(criterion => {
        return {
          criterion: criterion,
          signs: signslist.filter(sign => {
            return sign.criterion == criterion.id
          })
        };
      });
      await conn.end();
      return listForUser;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return;
  }

  let userInfo = await f_lib.db.userByLogin(data.userlogin);


  if (!isCompetenceCreatedByThisUser(data.competence, userInfo[0].id)) {
    res.send({
      status: false,
      content: 'Указанная компетенция создана не вами.'
    });
    return;
  };

  let listOfCriteria = await getCriteriaList(data.competence, userInfo[0].id);
  res.send({
    status: true,
    content: listOfCriteria
  });
}

async function redactCriteria(data, res) {
  data.criterion = f_lib.strings.changeDataNames(data.criterion);
  data.signs = data.signs.map(element => {
    return {
      name: f_lib.strings.changeDataNames(element.name),
      value: element.value
    };
  });

  function validate(data) {
    return (
      f_lib.strings.validateDataNums(data.id) &&
      f_lib.strings.validateLoginsPasswords(data.userlogin) &&
      f_lib.strings.validateDataNums(data.competence) &&
      f_lib.strings.validateDataNames(data.criterion) &&
      (data.criterionTypeCheckbox === true || data.criterionTypeCheckbox === false) &&
      data.signs.every(element => {
        return (f_lib.strings.validateDataNames(element.name) &&
          f_lib.strings.validateDataNums(element.value));
      })
    );
  }

  async function isCriterionCreatedByThisUser(id, user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`criteria\` WHERE \`id\` = '${id}' AND competence IN(
        SELECT id FROM competencies WHERE user = '${user}'
      )`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isCompetenceCreatedByThisUser(id, user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`competencies\` WHERE \`id\` = '${id}' AND \`user\` = ${user}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function redactCriterionInDB(data) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let resRedactCriterion = await conn.execute(`UPDATE criteria SET text='${data.criterion}', typecheckbox=${data.criterionTypeCheckbox ? 1 : 0} WHERE id = ${data.id} `);
      await conn.execute(`DELETE FROM signs WHERE criterion = ${data.id}`);
      for (let i = 0; i < data.signs.length; i++) {
        let addSign = await conn.execute(`INSERT INTO signs (criterion, text, weight) VALUES ('${data.id}', '${data.signs[i].name}', '${data.signs[i].value}')`);
      }

      let [criterion, fields] = await conn.execute(`SELECT * FROM criteria WHERE id = '${data.id}'`);
      let [signs, fields1] = await conn.execute(`SELECT * FROM signs WHERE criterion = '${data.id}'`);
      await conn.end();
      return {
        criterion: criterion[0],
        signs: signs
      };
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  let userInfo = await f_lib.db.userByLogin(data.userlogin);

  if (!await isCriterionCreatedByThisUser(data.id, userInfo[0].id)) {
    res.send({
      status: false,
      content: 'Критерий создан не вами.'
    });
    return false;
  }

  if (data.signs.length < 2) {
    res.send({
      status: false,
      content: 'Количество признаков не может быть меньше 2.'
    });
    return false;
  }

  if (!data.signs.every(element => element.value <= 100)) {
    res.send({
      status: false,
      content: 'Максимальное значение шкалы признака - 100.'
    });
    return false;
  }

  if (data.criterionTypeCheckbox === true) {
    let sumSigns = 0;
    data.signs.map(el => {
      sumSigns += el.value;
    });

    if (sumSigns != 100) {
      res.send({
        status: false,
        content: 'Для критерия с несколькими вариантами ответа суммарное значение по шкалам всех признаков должно составлять ровно 100.'
      });
      return false;
    }
  }

  if (!await isCompetenceCreatedByThisUser(data.competence, userInfo[0].id)) {
    res.send({
      status: false,
      content: 'Указанная компетенция создана не вами.'
    });
    return false;
  }

  let resredact = await redactCriterionInDB(data);

  res.send({
    status: true,
    content: resredact
  });
}

async function deleteCriteria(data, res) {
  function validate(data) {
    return (
      f_lib.strings.validateDataNums(data.id) &&
      f_lib.strings.validateLoginsPasswords(data.userlogin)
    );
  }

  async function isCriterionCreatedByThisUser(id, user) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`criteria\` WHERE \`id\` = '${id}' AND competence IN(
        SELECT id FROM competencies WHERE user = '${user}'
      )`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteCriterion(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM criteria WHERE id = ${id}`);;
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (!f_lib.objects.isSetData(data)) {
    res.send({
      status: false,
      content: 'Заполните все поля.'
    });
    return false;
  }

  if (!validate(data)) {
    res.send({
      status: false,
      content: 'Данные указаны некорректно.'
    });
    return false;
  }

  let userInfo = await f_lib.db.userByLogin(data.userlogin);

  if (!await isCriterionCreatedByThisUser(data.id, userInfo[0].id)) {
    res.send({
      status: false,
      content: 'Критерий создан не вами.'
    });
    return false;
  }

  await deleteCriterion(data.id);
  
  res.send({
    status: true,
    content: true
  });
}

module.exports.createCriteria = createCriteria;
module.exports.redactCriteria = redactCriteria;
module.exports.searchCriteria = searchCriteria;
module.exports.deleteCriteria = deleteCriteria;