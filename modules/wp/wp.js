const mysql = require('mysql2/promise');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');

async function addWP(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let newWProw = null;

  data.nameWP = f_lib.strings.changeDataNames(data.nameWP);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let wp = f_lib.strings.validateDataNames(data.nameWP);
    let copyWP = true;
    if (data.copyIdWP) {
      copyWP = f_lib.strings.validateDataNums(data.copyIdWP);
    }
    return login && wp && copyWP;
  }

  async function isSetWP(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function addWPtoDB(name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let result = await conn.execute(`INSERT INTO \`workpositions\` (\`user\`, \`name\`) VALUES ('${user[0].id}', '${name}');`);
      let getNewWPById = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${result[0].insertId}'`);
      await conn.end();
      return getNewWPById[0][0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isWPCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function makeCloneWP(baseWPid, cloneId, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rowsWPConnects, fieldsWPConnects] = await conn.execute(`SELECT * FROM \`wpconnections\` WHERE \`wp\` = '${baseWPid}'`);
      // rowsWPConnects.forEach(async element => {
      //   await conn.execute(`INSERT INTO \`wpconnections\` (\`wp\`, \`competence\`) VALUES ('${cloneId}', '${element.competence}');`);
      // });
      for(let i = 0; i < rowsWPConnects.length; i++){
        await conn.execute(`INSERT INTO \`wpconnections\` (\`wp\`, \`competence\`) VALUES ('${cloneId}', '${rowsWPConnects[i].competence}');`);
      }
      // await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetWP(data.nameWP, data.login)) {
        let newwp = await addWPtoDB(data.nameWP, data.login);
        newWProw = newwp;
        
        if (data.copyIdWP) {
          if (await isWPCreatedByThisUser(data.copyIdWP, data.login)) {
            if (data.nameWP != 'Должность не указана') {
              await makeCloneWP(data.copyIdWP, newWProw.id, data.login);
            }
            else {
              counterErrors = 1;
              errorsList.push('Это имя зарезервировано системой. Введите другое название должности.');
            }
          }
        }
      }
      else {
        counterErrors = 1;
        errorsList.push('Должность с таким названием уже существует.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название должности может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: newWProw
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function searchWP(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  data.nameWP = f_lib.strings.changeDataNames(data.nameWP);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let wp = f_lib.strings.validateDataNames(data.nameWP) || (data.nameWP == '');
    let searchByAlphabet = data.sortabc == 'true' || data.sortabc == 'false';
    return login && wp && searchByAlphabet;
  }

  async function searchWPinDB(name, login, sortabc) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`user\` = '${user[0].id}' AND \`name\` LIKE '%${name}%' ${sortabc == 'true' ? ' ORDER BY \`name\` ASC' : ''}`);
      await conn.end();
      return rows;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }
  let resultSearch = [];
  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      resultSearch = await searchWPinDB(data.nameWP, data.login, data.sortabc);
    }
    else {
      counterErrors = 1;
    }
  }
  else {
    counterErrors = 1;
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resultSearch
    });
  }
  else {
    res.send({
      status: false,
      content: resultSearch
    });
  }
}

async function deleteWP(data, res) {
  let counterErrors = 0;
  let errorsList = [];

  function validate(data) {
    let id = f_lib.strings.validateDataNums(data.id);
    let login = f_lib.strings.validateLoginsPasswords(data.login);
    return id && login;
  }

  async function isWPCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function deleteWorkPosition(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`DELETE FROM \`workpositions\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return true;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isWPCreatedByThisUser(data.id, data.login)) {
        await deleteWorkPosition(data.id);
      }
      else {
        counterErrors = 1;
        errorsList.push('У вас нет доступа для редактирования этих данных.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Данные указаны некорректно.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Данные не указаны.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: 'Данные удалены.'
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function renameWP(data, res) {
  let counterErrors = 0;
  let errorsList = [];
  let resrename = null;

  data.nameWP = f_lib.strings.changeDataNames(data.nameWP);

  function validate(data) {
    let login = f_lib.strings.validateNames(data.login);
    let wp = f_lib.strings.validateDataNames(data.nameWP);
    let id = f_lib.strings.validateDataNums(data.id);
    return login && wp && id;
  }

  async function isWPCreatedByThisUser(id, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}' AND \`user\` = ${user[0].id}`);
      await conn.end();
      return rows.length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetWPinDB(id, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.id == id
      }).length != 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function isSetWP(name, login) {
    let user = await f_lib.db.userByLogin(login);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`user\` = '${user[0].id}'`);
      await conn.end();
      return rows.filter(function (el, i) {
        return el.name == name
      }).length == 0;
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  async function renameWPinDB(id, name, userlogin) {
    let user = await f_lib.db.userByLogin(userlogin);
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`UPDATE \`workpositions\` SET \`name\`='${name}' WHERE \`id\` = '${id}' AND \`user\`= '${user[0].id}'`);
      let getNewWPById = await conn.execute(`SELECT * FROM \`workpositions\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return getNewWPById[0];
    } catch (error) {
      res.send({
        status: false,
        content: 'Не удалось установить соединение с базой данных. Попробуйте позже.'
      });
      throw error;
    }
  }

  if (f_lib.objects.isSetData(data)) {
    if (validate(data)) {
      if (await isSetWPinDB(data.id, data.login)) {
        if (await isSetWP(data.nameWP, data.login)) {
          if (await isWPCreatedByThisUser(data.id, data.login)) {
            if (data.nameWP != 'Должность не указана') {
              resrename = await renameWPinDB(data.id, data.nameWP, data.login);
            }
            else {
              counterErrors = 1;
              errorsList.push('Это имя зарезервировано системой. Введите другое название должности.');
            }
          }
          else {
            counterErrors = 1;
            errorsList.push('У вас нет доступа для редактирования этих данных.');
          }
        }
        else {
          counterErrors = 1;
          errorsList.push('Должность с таким названием уже существует.');
        }
      }
      else {
        counterErrors = 1;
        errorsList.push('Редактируемая должность не найдена.');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Название должности может состоять из букв русского, украинского и латинского алфавитов, цифр, пробелов, знаков подчёркивания.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: resrename
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

module.exports.addWP = addWP;
module.exports.searchWP = searchWP;
module.exports.deleteWP = deleteWP;
module.exports.renameWP = renameWP;