const mysql = require('mysql2/promise');
const moment = require('moment');
const conf = require('../config.js');
const f_lib = require('../func_lib.js');
const fs = require('fs');

async function userAvatar(login, basedir, res) {
  function validate(login) {
    return (
      f_lib.strings.validateLoginsPasswords(login) &&
      f_lib.strings.minLength(login, 6) &&
      f_lib.strings.maxLength(login, 30)
    );
  }

  async function userDirByLogin(login) {
    let conn = await mysql.createConnection(conf.dbconfig);
    let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${login}'`);
    await conn.end();
    if (rows.length > 0) {
      return rows[0].dir;
    }
    else {
      return false;
    }
  }

  function getAvatar(path, dir, res) {
    if (!fs.existsSync(`${dir}/users_file_system/${path}`)) {
      fs.mkdirSync(`${dir}/users_file_system/${path}/`);
      fs.mkdirSync(`${dir}/users_file_system/${path}/logo/`);
    }

    if (!fs.existsSync(`${dir}/users_file_system/${path}/logo/`)) {
      fs.mkdirSync(`${dir}/users_file_system/${path}/logo/`);
    }

    if (fs.existsSync(`${dir}/users_file_system/${path}/logo/`)) {
      let files = fs.readdirSync(`${dir}/users_file_system/${path}/logo/`);
      if (files.length > 0) {
        res.sendFile(`${dir}/users_file_system/${path}/logo/${files[0]}`)
      }
      else {
        res.sendFile(`${dir}/img/logo/user.png`);
      }
    }
    else {
      return false;
    }
    return true;
  }

  try {
    if (!f_lib.objects.isSetData({ login: login })) {
      throw 'Data is not defined!';
    }
    if (!validate(login)) {
      throw 'Invalid login!';
    }
    let userdir = await userDirByLogin(login);
    if (userdir) {
      getAvatar(userdir, basedir, res);
    }
    else {
      throw 'Can`t find user';
    }
  } catch (error) {
    throw error;
  }
}

async function uploadUserAvatar(login, oldpath, basedir, file, res) {

  function validate(login) {
    return (
      f_lib.strings.validateLoginsPasswords(login) &&
      f_lib.strings.minLength(login, 6) &&
      f_lib.strings.maxLength(login, 30)
    );
  }

  async function userDirByLogin(login) {
    let conn = await mysql.createConnection(conf.dbconfig);
    let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${login}'`);
    await conn.end();
    if (rows.length > 0) {
      return rows[0].dir;
    }
    else {
      return false;
    }
  }

  function fileType(file) {
    if (['image/png', 'image/jpeg'].includes(file.type)) {
      return true;
    }
    else {
      return false;
    }
  }

  async function emptyDir(path, dir, oldpath, file, res) {
    try {
      if (fs.existsSync(`${dir}/users_file_system/${path}/logo/`)) {
        let files = fs.readdirSync(`${dir}/users_file_system/${path}/logo/`);
        if (files.length > 0) {
          files.forEach((fileName, i) => {
            fs.unlinkSync(`${dir}/users_file_system/${path}/logo/${fileName}`, function (err) {
              if (err) {
                throw err;
              }
            });
          });
        }
        let newpath = `${dir}/users_file_system/${path}/logo/${file.name}`;
        fs.rename(oldpath, newpath, function (err) {
          if (err) throw err;
        });
      }
    }
    catch (errors) {
      throw errors;
    }
  }

  let counterErrors = false;
  let errorsList = [];

  if (f_lib.objects.isSetData({
    login: login,
    oldpath: oldpath,
    basedir: basedir,
    file: file
  })) {
    if (validate(login)) {
      let userDir = await userDirByLogin(login);
      if (fileType(file)) {
        if (userDir) {
          emptyDir(userDir, basedir, oldpath, file, res);
        }
      }
      else {
        counterErrors = 1;
        errorsList.push('Неверный формат файла. Используйте png/jpeg');
      }
    }
    else {
      counterErrors = 1;
      errorsList.push('Данные не прошли валидацию.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Ошибка обработки данных.');
  }

  if (counterErrors != 0) {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
  else {
    res.send({
      status: true,
      content: 'Фото загружено.'
    });
  }
}

async function getFullName(login, res) {
  let counterErrors = 0;
  let errorsMessege = [];

  function validate(login) {
    return (
      f_lib.strings.validateLoginsPasswords(login) &&
      f_lib.strings.minLength(login, 6) &&
      f_lib.strings.maxLength(login, 30)
    );
  }

  async function getTypeById(id) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let [rows, fields] = await conn.execute(`SELECT * FROM \`users_types\` WHERE \`id\` = '${id}'`);
      await conn.end();
      return rows[0].type;
    } catch (error) {
      message.total = 'Не удалось установить соединение с базой данных. Попробуйте позже.';
      message.status = false;
      res.send(message);
      throw error;
    }
  }

  async function userFullNameByLogin(login) {
    let conn = await mysql.createConnection(conf.dbconfig);
    let [rows, fields] = await conn.execute(`SELECT * FROM \`users\` WHERE \`login\` = '${login}'`);
    await conn.end();
    if (rows.length > 0) {
      return {
        name: rows[0].name,
        surename: rows[0].surename,
        patronymic: rows[0].patronymic,
        type: await getTypeById(rows[0].type)
      };
    }
    else {
      return false;
    }
  }

  let content = null;

  if (f_lib.objects.isSetData({ login: login })) {
    if (validate(login)) {
      content = await userFullNameByLogin(login);
    }
    else {
      counterErrors = 1;
      errorsMessege.push('Данные не прошли валидацию.');
    }
  }
  else {
    counterErrors = 1;
    errorsMessege.push('Данные указаны некорректно.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: content
    });
  }
  else {
    res.send({
      status: false,
      content: errorsMessege
    });
  }
}

async function changeUserFullName(login, name, res, col) {

  let counterErrors = 0;
  let errorsList = [];

  function validate(login, name, col) {
    let resName = f_lib.strings.minLength(name, 1) &&
      f_lib.strings.maxLength(name, 30) &&
      f_lib.strings.validateNames(name);
    if (!resName) {
      errorsList.push('Данные должны иметь длину не более 30 символов, состоять из букв русского, украинского или латинского алфавитов.');
    }
    let resLogin = f_lib.strings.validateLoginsPasswords(login) &&
      f_lib.strings.minLength(login, 6) &&
      f_lib.strings.maxLength(login, 30);
    if (!resLogin) {
      errorsList.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания. Допустимое количество символов в логине - от 6 до 30.');
    }

    let resCol = ['name', 'surename', 'patronymic'].includes(col);
    return resName && resLogin && resCol;
  }

  async function changeName(name, id, col) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      await conn.execute(`UPDATE \`users\` SET \`${col}\`='${name}' WHERE \`id\`='${id}'`);
      await conn.end();
      return true;
    }
    catch (error) {
      throw error;
    }
  }
  if (f_lib.objects.isSetData({
    login: login,
    name: name,
    col: col
  })) {
    if (validate(login, name, col)) {
      let userid = await f_lib.db.userByLogin(login);
      await changeName(name, userid[0].id, col);
    }
    else {
      counterErrors = 1;
      errorsList.push('Данные не прошли валидацию.');
    }
  }
  else {
    counterErrors = 1;
    errorsList.push('Данные указаны не полностью. Заполните все поля.');
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: 'Данные обновелны.'
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

async function changeUserPassword(login, oldpass, newpass, newpassrepeat, res) {
  let passwords = {
    login: login,
    oldpass: oldpass,
    newpass: newpass,
    newpassrepeat: newpassrepeat
  };

  let errorsList = [];
  let counterErrors = 0;

  function validate(passwords, login) {
    let resPasswords = passwords.every((pass, i) => {
      return (
        f_lib.strings.validateLoginsPasswords(pass) &&
        f_lib.strings.minLength(pass, 6) &&
        f_lib.strings.maxLength(pass, 30)
      );
    });

    return resPasswords;
  }

  async function oldPasswordTrue(oldpass, login) {
    let userid = await f_lib.db.userByLogin(login);
    let hashOldPass = f_lib.crypt.createSHA256(oldpass);
    return userid[0].password == hashOldPass;
  }

  function sameNewPasswords(newpass, newpassrepeat) {
    return newpass == newpassrepeat;
  }

  async function changePasword(pass, login) {
    try {
      let conn = await mysql.createConnection(conf.dbconfig);
      let userid = await f_lib.db.userByLogin(login);
      let hashNewPass = f_lib.crypt.createSHA256(pass);
      await conn.execute(`UPDATE \`users\` SET \`password\`='${hashNewPass}' WHERE \`id\`='${userid[0].id}'`);
      await conn.end();
      return true;
    }
    catch (error) {
      throw error;
    }
  }

  if (f_lib.objects.isSetData(passwords)) {
    if (validate([passwords.oldpass, passwords.newpass, passwords.newpassrepeat], passwords.login)) {
      if (await oldPasswordTrue(passwords.oldpass, passwords.login)) {
        if (sameNewPasswords(passwords.newpass, passwords.newpassrepeat)) {
          await changePasword(passwords.newpass, passwords.login);
        }
        else {
          errorsList.push('Пароли не совпадают.');
          counterErrors = 1;
        }
      }
      else {
        errorsList.push('Старый пароль указан неверно.');
        counterErrors = 1;
      }
    }
    else {
      errorsList.push('Пароли могут состоять из букв латинского алфавита, цифр и знака подчёркивания.');
      counterErrors = 1;
    }
  }
  else {
    errorsList.push('Заполните все поля.');
    counterErrors = 1;
  }

  if (counterErrors == 0) {
    res.send({
      status: true,
      content: 'Пароль изменён.'
    });
  }
  else {
    res.send({
      status: false,
      content: errorsList.join('')
    });
  }
}

module.exports.userAvatar = userAvatar;
module.exports.uploadUserAvatar = uploadUserAvatar;
module.exports.getFullName = getFullName;
module.exports.changeUserFullName = changeUserFullName;
module.exports.changeUserPassword = changeUserPassword;