const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const formidable = require('formidable');

const regUser = require('./modules/authuser/reguser.js');
const authUser = require('./modules/authuser/authuser.js');
const controlAuth = require('./modules/controlauth/controlAuth.js');
const userData = require('./modules/userdata/userdata.js');
const wpmodule = require('./modules/wp/wp.js');
const ugmodule = require('./modules/ug/ug.js');
const competenciesmodule = require('./modules/competencies/competencies.js');
const staffsmodule = require('./modules/staff/staffmodule.js');
const criteriamodule = require('./modules/criteria/criteriamodule.js');
const assessmentsModule = require('./modules/assessments/assessmentsmodule.js');
const fs = require('fs');

console.log('Server started...');

const issetusersdir = fs.existsSync(__dirname + '/users_file_system')

if(!issetusersdir) {
  fs.mkdirSync(__dirname + '/users_file_system')
}

const hrApp = express();
hrApp.use(cors({
  origin: '*',
  credentials: true
}));
hrApp.use(bodyParser.urlencoded({
  extended: true
}));
hrApp.use(bodyParser.json());
hrApp.use(cookieParser());

// const ipAddress = '127.0.0.1';
const expressPort = '5670';
const port = process.env.PORT || expressPort;

const urlEncoderParser = bodyParser.urlencoded({ extended: false });
try {
  hrApp.listen(port/*, ipAddress*/);
  console.log('Listening port 127.0.0.1:5670...');
  /* User identify operations */

  hrApp.post('/reguser', urlEncoderParser, function (req, res) {
    regUser.regUser(req.body, res);
  });

  hrApp.post('/authuser', urlEncoderParser, function (req, res) {
    authUser.authUser(req.body, res);
  });

  hrApp.get('/checktoken', urlEncoderParser, async function (req, res) {
    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      res.send(true);
    }
    else {
      res.send(false);
    }
  });

  /* Operations with user personal data */

  hrApp.get('/useravatar', urlEncoderParser, async function (req, res) {
    userData.userAvatar(req.query.login, __dirname, res);
  });

  hrApp.post('/uploadavatar', urlEncoderParser, function (req, res) {
    let form = new formidable.IncomingForm();
    form.parse(req, async (errors, fields, files) => {
      if (errors) {
        throw errors;
      }
      if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
        let oldpath = files.avatar.path;
        userData.uploadUserAvatar(req.headers.userlogin, oldpath, __dirname, files.avatar, res);
      }
      else {
        res.send({
          status: false,
          content: 'Вы не авторизованы.'
        });
      }
    });
  });

  hrApp.get('/getuserfullname', urlEncoderParser, async function (req, res) {
    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      userData.getFullName(req.headers.userlogin, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/changeusername', urlEncoderParser, async function (req, res) {
    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      await userData.changeUserFullName(req.headers.userlogin, req.body.updatename, res, req.body.updatefield);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/changeuserpassword', urlEncoderParser, async function (req, res) {
    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      userData.changeUserPassword(
        req.headers.userlogin,
        req.body.oldPassword,
        req.body.newPassword,
        req.body.newPasswordRepeat,
        res
      );
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  /* Work positions operations */

  hrApp.post('/addwp', urlEncoderParser, async function (req, res) {

    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      let copyIdWP = {};
      if (req.body.copyIdWP) {
        copyIdWP.copyIdWP = req.body.copyIdWP;
      }

      wpmodule.addWP({
        login: userLogin,
        nameWP: req.body.nameWP,
        ...copyIdWP
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/renamewp', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      wpmodule.renameWP({
        login: userLogin,
        id: req.body.id,
        nameWP: req.body.name
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.delete('/deletewp', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      wpmodule.deleteWP({
        login: userLogin,
        id: req.body.id
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchwp', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      wpmodule.searchWP({
        login: userLogin,
        nameWP: req.query.nameWP,
        sortabc: req.query.sortabc
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  /* users groups operations */

  hrApp.post('/addug', urlEncoderParser, async function (req, res) {

    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      let copyIdUG = {};
      if (req.body.copyIdUG) {
        copyIdUG.copyIdUG = req.body.copyIdUG;
      }

      ugmodule.addUG({
        login: userLogin,
        nameUG: req.body.nameUG,
        ...copyIdUG
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/renameug', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      ugmodule.renameUG({
        login: userLogin,
        id: req.body.id,
        nameUG: req.body.nameUG
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.delete('/deleteug', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      ugmodule.deleteUG({
        login: userLogin,
        id: req.body.id
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchug', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      ugmodule.searchUG({
        login: userLogin,
        nameUG: req.query.nameUG,
        sortabc: req.query.sortabc
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchusersgroupsconnections', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      ugmodule.searchConnects({
        login: userLogin,
        ug: req.query.ug
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  /* Competencies operations */

  hrApp.post('/addcompetence', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      competenciesmodule.addCompetence({
        login: userLogin,
        nameCompetence: req.body.nameCompetence
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/renamecompetence', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      competenciesmodule.renameCompetence({
        login: userLogin,
        id: req.body.id,
        nameCompetence: req.body.nameCompetence
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.delete('/deletecompetence', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      competenciesmodule.deleteCompetence({
        login: userLogin,
        id: req.body.id
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchcompetence', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      competenciesmodule.searchCompetence({
        login: userLogin,
        nameCompetence: req.query.nameCompetence,
        sortabc: req.query.sortabc
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.post('/changecompetenceconnecttowp', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      if (req.body.connect) {
        competenciesmodule.connectCompetenceToWP({
          login: userLogin,
          idwp: req.body.idwp,
          idcompetence: req.body.idcompetence,
          connect: true
        }, res);
      }
      else {
        competenciesmodule.disconnectCompetenceFromWP({
          login: userLogin,
          idwp: req.body.idwp,
          idcompetence: req.body.idcompetence,
          connect: false
        }, res);
      }
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchconnectswpcompetence', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      competenciesmodule.searchConnects({
        login: userLogin,
        wp: req.query.wp
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.post('/connectuserwithgroup', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      req.body.createByAdmin = req.headers.userlogin;
      ugmodule.connectUserToGroup({
        userlogin: userLogin,
        user: req.body.user,
        ug: req.body.ug
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  hrApp.post('/disconnectuserwithgroup', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      req.body.createByAdmin = req.headers.userlogin;
      ugmodule.disconnectUserFromGroup({
        userlogin: userLogin,
        user: req.body.user,
        ug: req.body.ug
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  /* staff operations */

  hrApp.post('/adduser', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      req.body.createByAdmin = req.headers.userlogin;
      regUser.regUser(req.body, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  hrApp.get('/getuserslist', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      staffsmodule.getStaffList({ login: userLogin }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.delete('/deleteuser', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      await staffsmodule.deleteUserFromSystem(__dirname, {
        login: userLogin,
        loginModer: req.headers.userlogin,
        deleteId: req.body.deleteId
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/changeusertype', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      await staffsmodule.changeUserType({
        login: userLogin,
        loginModer: req.headers.userlogin,
        changeUser: req.body.changeUser,
        typeModerator: req.body.typeModerator
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/getmoderatorsettings', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      await staffsmodule.getModeratorAccessList({
        login: userLogin,
        moderator: req.query.user
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/changemoderatoraccesspunkts', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      await staffsmodule.changeModeratorAccessList({
        checkedstatus: req.body.checkedstatus,
        login: userLogin,
        functional: req.body.functional,
        moderator: req.body.moderator,
        loginAuthUser: req.headers.userlogin
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.post('/connectuserwithwp', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      req.body.createByAdmin = req.headers.userlogin;
      staffsmodule.connectUserWithWP({
        userlogin: userLogin,
        user: req.body.user,
        wp: req.body.wp
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  hrApp.delete('/disconnectuserandwp', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactstaff', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      req.body.createByAdmin = req.headers.userlogin;
      staffsmodule.disconnectUserAndWP({
        userlogin: userLogin,
        user: req.body.user
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  /* criteria operations */

  hrApp.post('/addcriterion', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      criteriamodule.createCriteria({
        userlogin: userLogin,
        competence: req.body.competence,
        criterion: req.body.criterion,
        criterionTypeCheckbox: req.body.criterionTypeCheckbox,
        signs: req.body.signs
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  hrApp.patch('/redactcriterion', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      criteriamodule.redactCriteria({
        id: req.body.id,
        userlogin: userLogin,
        competence: req.body.competence,
        criterion: req.body.criterion,
        criterionTypeCheckbox: req.body.criterionTypeCheckbox,
        signs: req.body.signs
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.delete('/deletecriterion', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      criteriamodule.deleteCriteria({
        id: req.body.id,
        userlogin: userLogin
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchcriteria', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'redactforms', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      criteriamodule.searchCriteria({
        userlogin: userLogin,
        competence: req.query.competence
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  /* assessments operations */

  hrApp.post('/createassessment', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'makeevents', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.createAssessment({
        userlogin: userLogin,
        usersInvites: req.body.usersInvites,
        nameAssessment: req.body.nameAssessment
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }

  });

  hrApp.get('/getinviteslist', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator', 'user']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.getuserInvites({
        userlogin: userLogin
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/getformbyinvite', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator', 'user']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.getformbyinvite({
        userlogin: userLogin,
        inviteId: req.query.invite
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.post('/setresults', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator', 'user']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.setresults({
        userlogin: userLogin,
        invite: req.body.invite,
        results: req.body.results, 
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/searchassessments', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'makeevents', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.getAssessmentsList({
        login: userLogin,
        nameAssessment: req.query.nameAssessment,
        sortabc: req.query.sortabc
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.patch('/renameAssessment', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'makeevents', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.renameAssessment({
        login: userLogin,
        id: req.body.id,
        nameassessment: req.body.nameassessment
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.delete('/deleteassessment', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'makeevents', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.deleteassessment({
        login: userLogin,
        id: req.body.id
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/assessmentresults', urlEncoderParser, async function (req, res) {
    let checkTypeOfUser = await controlAuth.checkUserType(req.headers.userlogin, ['admin', 'moderator']);
    if (!checkTypeOfUser) {
      await controlAuth.sendUserTypeError(res);
      return;
    }

    let userLogin = req.headers.userlogin;

    if (checkTypeOfUser == 'moderator') {
      userLogin = await controlAuth.checkModerAccess(req.headers.userlogin, res);
      if (!userLogin) {
        await controlAuth.sendUserTypeError(res);
        return;
      }
      let resAccess = await controlAuth.checkAccessToFunctional(req.headers.userlogin, 'makeevents', res);
      if (!resAccess) {
        await controlAuth.sendUserAccessError(res);
        return;
      }
    }

    if (await controlAuth.checkToken({ login: req.headers.userlogin, token: req.headers.usertoken }, res)) {
      assessmentsModule.getassessmentresults({
        login: userLogin,
        assessment: req.query.assessment
      }, res);
    }
    else {
      res.send({
        status: false,
        content: 'Вы не авторизованы.'
      });
    }
  });

  hrApp.get('/image_base', urlEncoderParser, function (req, res) {
    res.sendFile(__dirname + '/img/' + req.query.path);
  });
} catch(e) {
  console.log(e)
}